package com.example.samogon.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.samogon.data.local.db.dao.*
import com.example.samogon.data.local.model.*
import com.example.samogon.data.local.model.crossref.CocktailComponentCrossRef
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.local.model.crossref.CocktailTasteCrossRef
import com.example.samogon.data.local.model.crossref.CompilationCocktailCrossRef
import com.example.samogon.utils.converter.TasteConverter

@Database(
    entities = [Cocktail::class, Taste::class, Step::class, Component::class, Ingredient::class, Base::class, Strength::class, Difficulty::class, Compilation::class, CocktailTasteCrossRef::class, CocktailStepCrossRef::class, CocktailComponentCrossRef::class, CompilationCocktailCrossRef::class],
    version = 24,
    exportSchema = false
)
@TypeConverters(TasteConverter::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun cocktailDao(): CocktailDao
    //abstract fun cocktailFtsDao(): CocktailFtsDao
    abstract fun tasteDao(): TasteDao
    abstract fun stepDao(): StepDao
    abstract fun componentDao(): ComponentDao
    abstract fun ingredientDao(): IngredientDao
    abstract fun baseDao(): BaseDao
    abstract fun strengthDao(): StrengthDao
    abstract fun difficultyDao(): DifficultyDao
    abstract fun compilationDao(): CompilationDao
}