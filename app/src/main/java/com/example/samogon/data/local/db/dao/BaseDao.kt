package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.samogon.data.local.model.Base
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Taste


@Dao
interface BaseDao {

    @Query("SELECT * FROM bases ORDER BY baseId ASC")
    suspend fun getAll(): List<Base>

    @Query("SELECT * FROM bases ORDER BY baseId ASC")
    fun getBases(): PagingSource<Int, Base>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Base>)

    @Query("DELETE FROM bases")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM bases")
    suspend fun getCount(): Int?

    @Query("SELECT * FROM bases WHERE baseId=:id")
    suspend fun getBase(id: String): Base
}