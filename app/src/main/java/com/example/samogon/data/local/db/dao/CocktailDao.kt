package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.CocktailWithComponents
import com.example.samogon.data.local.model.CocktailWithSteps
import com.example.samogon.data.local.model.CocktailWithTastes
import com.example.samogon.data.local.model.crossref.CocktailComponentCrossRef
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.local.model.crossref.CocktailTasteCrossRef


@Dao
interface CocktailDao {

    @Query("SELECT * FROM cocktails ORDER BY cocktailId ASC")
    fun getCocktails(): PagingSource<Int, Cocktail>

    @Query("SELECT * FROM cocktails WHERE name LIKE '%' || CASE Length(:searchClause) WHEN 0 THEN NULL ELSE :searchClause END || '%' OR description LIKE '%' || CASE Length(:searchClause) WHEN 0 THEN NULL ELSE :searchClause END || '%' ORDER BY cocktailId ASC")
    fun searchCocktails(searchClause: String?): PagingSource<Int, Cocktail>

    @Transaction
    @Query("SELECT * FROM cocktails ORDER BY cocktailId ASC")
    fun getCocktailsWithTastes(): PagingSource<Int, CocktailWithTastes>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Cocktail>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: Cocktail)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllWithTastes(items: List<CocktailTasteCrossRef>)

    @Query("DELETE FROM cocktails")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM cocktails")
    suspend fun getCount(): Int?

    @Query("SELECT COUNT(*) FROM cocktails WHERE name LIKE '%' || CASE Length(:searchClause) WHEN 0 THEN NULL ELSE :searchClause END || '%' OR description LIKE '%' || CASE Length(:searchClause) WHEN 0 THEN NULL ELSE :searchClause END || '%'")
    suspend fun getCount(searchClause: String): Int?

    @Query("SELECT * FROM cocktails WHERE cocktailId=:id")
    suspend fun getCocktail(id: String): Cocktail

    @Transaction
    @Query("SELECT * FROM cocktails WHERE cocktailId=:id")
    suspend fun getCocktailWithSteps(id: String): CocktailWithSteps?

    @Transaction
    @Query("SELECT * FROM cocktails WHERE cocktailId=:id")
    suspend fun getCocktailWithComponents(id: String): CocktailWithComponents?

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWithSteps(item: CocktailStepCrossRef)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWithComponents(item: CocktailComponentCrossRef)
}