package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.example.samogon.data.local.model.*
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.local.model.crossref.CompilationCocktailCrossRef


@Dao
interface CompilationDao {

    @Query("SELECT * FROM compilations ORDER BY compilationId ASC")
    suspend fun getAll(): List<Compilation>

    @Query("SELECT * FROM compilations ORDER BY compilationId ASC")
    fun getCompilations(): PagingSource<Int, Compilation>

    @Transaction
    @Query("SELECT * FROM compilations ORDER BY compilationId ASC")
    fun getCompilationsWithCocktails(): List<CompilationWithCocktails>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: Compilation)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Compilation>)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWithCocktails(item: CompilationCocktailCrossRef)

    @Query("DELETE FROM compilations")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM compilations")
    suspend fun getCount(): Int?

    @Query("SELECT * FROM compilations WHERE compilationId=:id")
    suspend fun getCompilation(id: String): Compilation

    @Transaction
    @Query("SELECT * FROM compilations WHERE compilationId=:id")
    suspend fun getCompilationWithCocktails(id: String): CompilationWithCocktails?
}