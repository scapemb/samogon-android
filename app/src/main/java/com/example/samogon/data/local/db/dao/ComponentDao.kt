package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Component
import com.example.samogon.data.local.model.Step
import com.example.samogon.data.local.model.Taste


@Dao
interface ComponentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Component>)

    @Query("DELETE FROM components")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM components")
    suspend fun getCount(): Int?

    @Query("SELECT * FROM components WHERE componentId=:id")
    suspend fun getComponent(id: String): Component
}