package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.samogon.data.local.model.*


@Dao
interface DifficultyDao {

    @Query("SELECT * FROM difficulties ORDER BY difficulty_value ASC")
    suspend fun getAll(): List<Difficulty>

    @Query("SELECT * FROM difficulties ORDER BY difficulty_value ASC")
    fun getDifficulties(): PagingSource<Int, Difficulty>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Difficulty>)

    @Query("DELETE FROM difficulties")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM difficulties")
    suspend fun getCount(): Int?

    @Query("SELECT * FROM difficulties WHERE difficultyId=:id")
    suspend fun getDifficulty(id: String): Difficulty
}