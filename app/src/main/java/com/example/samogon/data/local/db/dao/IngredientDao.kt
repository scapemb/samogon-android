package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.example.samogon.data.local.model.*
import com.example.samogon.data.local.model.crossref.CocktailComponentCrossRef
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.local.model.crossref.CocktailTasteCrossRef


@Dao
interface IngredientDao {

    @Query("SELECT * FROM ingredients ORDER BY name ASC")
    fun getIngredients(): PagingSource<Int, Ingredient>

    @Query("SELECT * FROM ingredients WHERE name LIKE '%' || CASE Length(:searchClause) WHEN 0 THEN NULL ELSE :searchClause END || '%' ORDER BY name ASC")
    fun searchIngredients(searchClause: String?): PagingSource<Int, Ingredient>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Ingredient>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: Ingredient)

    @Query("DELETE FROM ingredients")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM ingredients")
    suspend fun getCount(): Int?

    @Query("SELECT COUNT(*) FROM ingredients WHERE name LIKE '%' || CASE Length(:searchClause) WHEN 0 THEN NULL ELSE :searchClause END || '%'")
    suspend fun getCount(searchClause: String): Int?

    @Query("SELECT * FROM ingredients WHERE ingredientId=:id")
    suspend fun getIngredient(id: String): Ingredient
}