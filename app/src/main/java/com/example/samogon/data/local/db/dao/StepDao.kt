package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Step
import com.example.samogon.data.local.model.Taste


@Dao
interface StepDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Step>)

    @Query("DELETE FROM steps")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM steps")
    suspend fun getCount(): Int?

    @Query("SELECT * FROM steps WHERE stepId=:id")
    suspend fun getStep(id: String): Step
}