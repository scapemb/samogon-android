package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.samogon.data.local.model.Base
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Strength
import com.example.samogon.data.local.model.Taste


@Dao
interface StrengthDao {

    @Query("SELECT * FROM strengths ORDER BY strength_value ASC")
    suspend fun getAll(): List<Strength>

    @Query("SELECT * FROM strengths ORDER BY strength_value ASC")
    fun getBases(): PagingSource<Int, Strength>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Strength>)

    @Query("DELETE FROM strengths")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM strengths")
    suspend fun getCount(): Int?

    @Query("SELECT * FROM strengths WHERE strengthId=:id")
    suspend fun getStrength(id: String): Strength
}