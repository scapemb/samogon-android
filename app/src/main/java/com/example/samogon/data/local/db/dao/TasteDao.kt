package com.example.samogon.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Taste


@Dao
interface TasteDao {

    @Query("SELECT * FROM tastes ORDER BY tasteId ASC")
    fun getTastes(): PagingSource<Int, Taste>

    @Query("SELECT * FROM tastes ORDER BY tasteId ASC")
    suspend fun getAll(): List<Taste>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<Taste>)

    @Query("DELETE FROM tastes")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM tastes")
    suspend fun getCount(): Int?

    @Query("SELECT * FROM tastes WHERE tasteId=:id")
    suspend fun getTaste(id: String): Taste
}