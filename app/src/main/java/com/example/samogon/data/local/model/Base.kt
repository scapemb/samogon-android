package com.example.samogon.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "bases")
@JsonClass(generateAdapter = true)
data class Base (
    @PrimaryKey @Json(name = "objectId") val baseId: String,
    @Json(name = "name") @ColumnInfo(name = "base_name") val name: String,
    @Json(name = "value") @ColumnInfo(name = "base_value") val value: Int,
    @Json(name = "isMainPart") val isMainPart: Boolean = false,
    @Ignore override var isSelected: Boolean = false
) : Selectable (isSelected) {
    constructor(baseId: String, name: String, value: Int, isMainPart: Boolean) :
        this(baseId, name, value, isMainPart, false)
}