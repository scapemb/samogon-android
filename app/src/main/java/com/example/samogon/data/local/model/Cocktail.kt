package com.example.samogon.data.local.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.samogon.utils.converter.TasteConverter
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "cocktails")
@JsonClass(generateAdapter = true)
data class Cocktail (
    @PrimaryKey @Json(name = "objectId") val cocktailId: String,
    @Json(name = "name") val name: String,
    @Json(name = "image") val image: String?,
    @Json(name = "description") val description: String,
    @Json(name = "preparation_time") val preparationTime: Int,
    @Json(name = "volume") val volume: Int,
    @Json(name = "strength") @Embedded(prefix = "str") val strength: Strength? = null,
    @Json(name = "difficulty") @Embedded(prefix = "diff") val difficulty: Difficulty? = null,
    @Json(name = "taste") @TypeConverters(TasteConverter::class) val taste: List<Taste>? = null
)