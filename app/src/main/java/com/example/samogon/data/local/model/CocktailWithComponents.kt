package com.example.samogon.data.local.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.samogon.data.local.model.crossref.CocktailComponentCrossRef
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.local.model.crossref.CocktailTasteCrossRef


data class CocktailWithComponents (
    @Embedded val cocktail: Cocktail,
    @Relation(
        parentColumn = "cocktailId",
        entityColumn = "componentId",
        associateBy = Junction(CocktailComponentCrossRef::class)
    )
    val components: List<Component>
)
