package com.example.samogon.data.local.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.local.model.crossref.CocktailTasteCrossRef


data class CocktailWithSteps (
    @Embedded val cocktail: Cocktail,
    @Relation(
        parentColumn = "cocktailId",
        entityColumn = "stepId",
        associateBy = Junction(CocktailStepCrossRef::class)
    )
    val steps: List<Step>
)
