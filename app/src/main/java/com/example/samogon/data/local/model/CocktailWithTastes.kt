package com.example.samogon.data.local.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.samogon.data.local.model.crossref.CocktailTasteCrossRef


data class CocktailWithTastes (
    @Embedded val cocktail: Cocktail,
    @Relation(
        parentColumn = "cocktailId",
        entityColumn = "tasteId",
        associateBy = Junction(CocktailTasteCrossRef::class)
    )
    val tastes: List<Taste>
)
