package com.example.samogon.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.samogon.data.remote.model.CompilationRemote
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "compilations")
@JsonClass(generateAdapter = true)
data class Compilation (
    @PrimaryKey @Json(name = "objectId") val compilationId: String,
    @Json(name = "name") @ColumnInfo(name = "compilation_name") val name: String
) {
    constructor(remote: CompilationRemote): this(remote.compilationId, remote.name)
}