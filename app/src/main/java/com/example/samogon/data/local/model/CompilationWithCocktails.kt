package com.example.samogon.data.local.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.samogon.data.local.model.crossref.CompilationCocktailCrossRef

data class CompilationWithCocktails (
    @Embedded val compilation: Compilation,
    @Relation(
        parentColumn = "compilationId",
        entityColumn = "cocktailId",
        associateBy = Junction(CompilationCocktailCrossRef::class)
    )
    val cocktails: List<Cocktail>
)
