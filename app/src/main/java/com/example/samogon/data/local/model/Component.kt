package com.example.samogon.data.local.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "components")
@JsonClass(generateAdapter = true)
data class Component (
    @PrimaryKey @Json(name = "objectId") val componentId: String,
    @Json(name = "value") val value: Int,
    @Json(name = "value_type") val valueType: Int,
    @Json(name = "ingredient") @Embedded val ingredient: Ingredient
)