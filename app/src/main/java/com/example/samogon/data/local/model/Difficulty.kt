package com.example.samogon.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "difficulties")
@JsonClass(generateAdapter = true)
data class Difficulty (
    @PrimaryKey @Json(name = "objectId") val difficultyId: String,
    @Json(name = "name") @ColumnInfo(name = "difficulty_name") val name: String,
    @Json(name = "description") @ColumnInfo(name = "difficulty_description") val description: String,
    @Json(name = "value") @ColumnInfo(name = "difficulty_value") val value: Int,
    @ColumnInfo(name = "difficulty_selected") var isSelected: Boolean = false
)