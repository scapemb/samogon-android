package com.example.samogon.data.local.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * types:
 * 1xx - alcohol
 * 2xx - juice
 * 3xx - fruit
 * 4xx - syrup
 * 9xx - additional
 * */
@Entity(tableName = "ingredients")
@JsonClass(generateAdapter = true)
data class Ingredient (
    @PrimaryKey @Json(name = "objectId") val ingredientId: String,
    @Json(name = "name") val name: String,
    @Json(name = "image") val image: String,
    @Json(name = "type") val type: Int,
    @Json(name = "base") @Embedded val base: Base?
) {
    companion object{
        fun generatePreview() = Ingredient("", "Click to select", "",999, null)
    }

    fun isLiquid() : Boolean {
        val typeGroup = type / 100
        return typeGroup < 4
    }
}
