package com.example.samogon.data.local.model

abstract class Selectable(selectable: Boolean) {
    open var isSelected: Boolean = selectable
}