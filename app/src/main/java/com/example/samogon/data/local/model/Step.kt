package com.example.samogon.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "steps")
@JsonClass(generateAdapter = true)
data class Step (
    @PrimaryKey @Json(name = "objectId") val stepId: String,
    @Json(name = "description") val description: String,
    @Json(name = "number") val number: Int
)