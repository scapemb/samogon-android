package com.example.samogon.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "strengths")
@JsonClass(generateAdapter = true)
data class Strength (
    @PrimaryKey @Json(name = "objectId") val strengthId: String,
    @Json(name = "name") @ColumnInfo(name = "strength_name") val name: String,
    @Json(name = "value") @ColumnInfo(name = "strength_value") val value: Int,
    @ColumnInfo(name = "strength_selected") var isSelected: Boolean = false
)