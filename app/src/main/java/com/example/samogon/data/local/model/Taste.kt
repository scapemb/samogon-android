package com.example.samogon.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "tastes")
@JsonClass(generateAdapter = true)
data class Taste (
    @PrimaryKey @Json(name = "objectId") val tasteId: String,
    @Json(name = "name")  @ColumnInfo(name = "taste_name") val name: String,
    @Ignore override var isSelected: Boolean = false
) : Selectable(isSelected) {
    constructor(tasteId: String, name: String) : this(tasteId, name, false)
}