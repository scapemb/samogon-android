package com.example.samogon.data.local.model.crossref

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["cocktailId", "componentId"])
data class  CocktailComponentCrossRef(
    val cocktailId: String,
    val componentId: String
)