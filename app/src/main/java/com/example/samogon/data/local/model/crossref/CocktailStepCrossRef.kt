package com.example.samogon.data.local.model.crossref

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["cocktailId", "stepId"])
data class  CocktailStepCrossRef(
    val cocktailId: String,
    val stepId: String
)