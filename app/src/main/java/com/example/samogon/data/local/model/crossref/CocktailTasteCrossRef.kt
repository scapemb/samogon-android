package com.example.samogon.data.local.model.crossref

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["cocktailId", "tasteId"])
data class  CocktailTasteCrossRef(
    //@PrimaryKey(autoGenerate = true) val id: String,
    val cocktailId: String,
    val tasteId: String
)