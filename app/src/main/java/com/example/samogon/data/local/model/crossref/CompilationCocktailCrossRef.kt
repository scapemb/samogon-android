package com.example.samogon.data.local.model.crossref

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["compilationId", "cocktailId"])
data class  CompilationCocktailCrossRef(
    val compilationId: String,
    val cocktailId: String
)