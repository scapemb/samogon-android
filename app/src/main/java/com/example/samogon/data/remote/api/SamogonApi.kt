package com.example.samogon.data.remote.api

import com.example.samogon.data.remote.model.CreateCocktailFullRemote
import javax.inject.Inject

class SamogonApi @Inject constructor(private val service: SamogonService) {
    companion object {
        const val BASE_URL = "https://api.backendless.com/93D5848C-E8B9-09EC-FF7B-E500D4035600/7CE32610-941B-42FD-862D-DAA8A50EE142/"
        const val PAGING_SIZE = 20
    }

    suspend fun fetchCocktails(pageSize: Int = PAGING_SIZE, offset: Int) = service.fetchCocktailList(pageSize, offset)

    suspend fun createCocktail(cocktail: CreateCocktailFullRemote) = service.createCocktail(cocktail)

    suspend fun searchCocktails(searchClause: String, pageSize: Int = PAGING_SIZE, offset: Int) = service.searchCocktails(searchClause, pageSize, offset)

    suspend fun fetchCocktailsForIngredient(ingredientId: String, pageSize: Int = PAGING_SIZE, offset: Int) = service.fetchCocktailListForIngredient(ingredientId, pageSize, offset)

    suspend fun fetchCocktailsForCompilation(compilationId: String, pageSize: Int = PAGING_SIZE, offset: Int) = service.fetchCocktailListForCompilation(compilationId, pageSize, offset)

    suspend fun fetchCocktail(id: String) = service.fetchCocktail(id)

    suspend fun fetchTastes(cocktailId: String) = service.fetchTasteList(cocktailId)

    suspend fun fetchTastes() = service.fetchTasteList()

    suspend fun fetchBases() = service.fetchBaseList()

    suspend fun fetchDifficulties() = service.fetchDifficultyList()

    suspend fun fetchStrengths() = service.fetchStrengthList()

    suspend fun fetchCompilations() = service.fetchCompilationList()

    suspend fun fetchSteps(cocktailId: String) = service.fetchSteps(cocktailId)

    suspend fun fetchComponents(cocktailId: String) = service.fetchComponents(cocktailId)

    suspend fun fetchIngredients(pageSize: Int = PAGING_SIZE, offset: Int) = service.fetchIngredientList(pageSize, offset)

    suspend fun searchIngredients(searchClause: String, pageSize: Int = PAGING_SIZE, offset: Int) = service.searchIngredients(searchClause, pageSize, offset)

    suspend fun fetchCocktailsForBases(bases: List<String>, pageSize: Int = PAGING_SIZE, offset: Int) = service.fetchCocktailListForBases(bases.joinToString(separator = ","), pageSize, offset)

    suspend fun filterCocktails(bases: List<String>?, tastes: List<String>?, difficulties: List<String>?, strengths: List<String>?,
                                pageSize: Int = PAGING_SIZE, offset: Int)
    = service.filterCocktails(
        bases?.joinToString(separator = ","),
        tastes?.joinToString(separator = ","),
        difficulties?.joinToString(separator = ","),
        strengths?.joinToString(separator = ","),
        pageSize,
        offset
    )
}