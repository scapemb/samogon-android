package com.example.samogon.data.remote.api

import com.example.samogon.data.local.model.*
import com.example.samogon.data.remote.model.CompilationRemote
import com.example.samogon.data.remote.model.CreateCocktailFullRemote
import com.example.samogon.data.remote.model.CreateCocktailRemote
import retrofit2.http.*

interface SamogonService {

    @GET("data/cocktail")
    suspend fun fetchCocktailList(
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int,
        @Query("relationsDepth") relationsDepth: Int = 1,
        @Query("sortBy") sortBy: String = "objectId"
    ): List<Cocktail>

    @GET("services/service/ingredientcocktails")
    suspend fun fetchCocktailListForIngredient(
        @Query("ingredientId") ingredientId: String,
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int
    ): List<Cocktail>

    @GET("services/service/compilationcocktails")
    suspend fun fetchCocktailListForCompilation(
        @Query("compilationId") compilationId: String,
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int
    ): List<Cocktail>

    @GET("data/cocktail/{objectId}")
    suspend fun fetchCocktail(
        @Path("objectId") id: String,
        @Query("relationsDepth") relationsDepth: Int = 1
    ): Cocktail

    @GET("services/service/cocktailtastes")
    suspend fun fetchTasteList(
        @Query("cocktailId") cocktailId: String,
        @Query("sortBy") sortBy: String = "objectId"
    ): List<Taste>

    @GET("data/taste")
    suspend fun fetchTasteList(
        @Query("sortBy") sortBy: String = "objectId"
    ): List<Taste>

    @GET("data/base")
    suspend fun fetchBaseList(
        @Query("sortBy") sortBy: String = "objectId"
    ): List<Base>

    @GET("data/difficulty")
    suspend fun fetchDifficultyList(
        @Query("sortBy") sortBy: String = "objectId"
    ): List<Difficulty>

    @GET("data/strength")
    suspend fun fetchStrengthList(
        @Query("sortBy") sortBy: String = "objectId"
    ): List<Strength>

    @GET("data/compilation")
    suspend fun fetchCompilationList(
        @Query("sortBy") sortBy: String = "objectId",
        @Query("relationsDepth") relationsDepth: Int = 1
    ): List<CompilationRemote>

    @GET("services/service/cocktailsteps")
    suspend fun fetchSteps(
        @Query("cocktailId") cocktailId: String
    ): List<Step>

    @GET("services/service/cocktailcomponents")
    suspend fun fetchComponents(
        @Query("cocktailId") cocktailId: String
    ): List<Component>

    @GET("services/service/cocktailsearch")
    suspend fun searchCocktails(
        @Query("clause") searchClause: String,
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int,
        @Query("relationsDepth") relationsDepth: Int = 1
    ): List<Cocktail>

    @GET("data/ingredient")
    suspend fun fetchIngredientList(
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int,
        @Query("sortBy") sortBy: String = "objectId"
    ): List<Ingredient>

    @GET("services/service/ingredientsearch")
    suspend fun searchIngredients(
        @Query("clause") searchClause: String,
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int
    ): List<Ingredient>


    @GET("services/service/basescocktails")
    suspend fun fetchCocktailListForBases(
        @Query("baseIds") bases: String?,
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int,
        @Query("relationsDepth") relationsDepth: Int = 1
    ): List<Cocktail>

    @GET("services/service/cocktailfilter")
    suspend fun filterCocktails(
        @Query("baseIds") bases: String?,
        @Query("tasteIds") tastes: String?,
        @Query("difficultyIds") difficulties: String?,
        @Query("strengthIds") strengths: String?,
        @Query("pageSize") pageSize: Int,
        @Query("offset") offset: Int
    ): List<Cocktail>

    @POST("services/service/cocktail")
    suspend fun createCocktail(
        @Body cocktail: CreateCocktailFullRemote
    ): Cocktail
}