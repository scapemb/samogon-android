package com.example.samogon.data.remote.model

import com.squareup.moshi.Json

data class CocktailRemote (
    @Json(name = "objectId") val id: String,
    @Json(name = "name") val name: String,
    @Json(name = "description") val description: String
)