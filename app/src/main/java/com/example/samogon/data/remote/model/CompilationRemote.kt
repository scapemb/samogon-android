package com.example.samogon.data.remote.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.example.samogon.data.local.model.Cocktail
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CompilationRemote (
    @Json(name = "objectId") val compilationId: String,
    @Json(name = "name") val name: String,
    @Json(name = "cocktails") val cocktails: List<Cocktail>
)