package com.example.samogon.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CreateCocktailFullRemote (
    val cocktail: CreateCocktailRemote,
    val components: List<CreateComponentRemote>,
    val steps: List<CreateStepRemote>
)