package com.example.samogon.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CreateCocktailRemote (
    val name: String,
    val description: String,
    val image: String = "",
    val volume: Int,
    @Json(name = "preparation_time") val preparationTime: Int,
    val difficultyId: String,
    val strengthId: String
)