package com.example.samogon.data.remote.model

import com.example.samogon.data.local.model.Ingredient
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CreateComponentRemote (
    val ingredientId: String,
    var value: Int = 0,
    @Json(name = "value_type") var valueType: Int = 0,
    @Transient val volume: Int = 0,                     //just for ml counting in time of creation. Value of parts will counted later
    @Transient val ingredient: Ingredient = Ingredient.generatePreview()        //for UI
) {
    fun isLiquid() : Boolean = ingredient.isLiquid()
}