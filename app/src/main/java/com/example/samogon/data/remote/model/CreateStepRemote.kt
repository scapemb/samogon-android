package com.example.samogon.data.remote.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CreateStepRemote (
    val description: String,
    val number: Int
)