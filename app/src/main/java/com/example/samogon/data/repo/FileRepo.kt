package com.example.samogon.data.repo

import com.backendless.Backendless
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.io.File

class FileRepo(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun uploadImage(image: File) = Backendless.Files.upload(image, "/img")
}