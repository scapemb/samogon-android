package com.example.samogon.data.repo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Difficulty
import com.example.samogon.data.local.model.Strength
import com.example.samogon.data.remote.api.SamogonApi
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.data.remote.model.CreateStepRemote
import com.example.samogon.data.repo.datasource.*
import com.example.samogon.data.repo.interactor.CreateCocktailInteractor
import com.example.samogon.utils.enums.SearchMode
import java.io.File

class SamogonRepo (
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    private val cocktailsRemoteMediator by lazy { CocktailsRemoteMediator(api, db) }
    private val ingredientsRemoteMediator by lazy { IngredientsRemoteMediator(api, db) }
    private val cocktailsSearchRemoteMediator by lazy { CocktailsRemoteMediator(api, db, true) }
    private val ingredientsSearchRemoteMediator by lazy { IngredientsRemoteMediator(api, db, true) }
    private val cocktailDataSource by lazy { CocktailDataSource(api, db) }
    private val baseDataSource by lazy { BasesDataSource(api, db) }
    private val strengthsDataSource by lazy { StrengthsDataSource(api, db) }
    private val difficultiesDataSource by lazy { DifficultiesDataSource(api, db) }
    private val tasteDataSource by lazy { TastesDataSource(api, db) }
    private val stepsDataSource by lazy { StepsDataSource(api, db) }
    private val componentsDataSource by lazy { ComponentsDataSource(api, db) }
    private val compilationsDataSource by lazy { CompilationsDataSource(api, db) }
    private val createCocktailInteractor by lazy { CreateCocktailInteractor(api, db) }

    val cocktails = Pager(
        config = PagingConfig(SamogonApi.PAGING_SIZE),
        remoteMediator = cocktailsRemoteMediator,
        pagingSourceFactory = { db.cocktailDao().getCocktails() }
    ).flow

    val ingredients = Pager(
        config = PagingConfig(SamogonApi.PAGING_SIZE),
        remoteMediator = ingredientsRemoteMediator,
        pagingSourceFactory = { db.ingredientDao().getIngredients() }
    ).flow

    var searchMode: SearchMode = SearchMode.TEXT

    var searchClause: String? = null
        set(value) {
            field = value
            cocktailsSearchRemoteMediator.searchClause = value
            ingredientsSearchRemoteMediator.searchClause = value
        }

    var bases: List<String>? = null
    var tastes: List<String>? = null
    var strengths: List<String>? = null
    var difficulties: List<String>? = null

    val searchCocktails = Pager(
        config = PagingConfig(SamogonApi.PAGING_SIZE),
        remoteMediator = cocktailsSearchRemoteMediator,
        pagingSourceFactory = { db.cocktailDao().searchCocktails(searchClause) }
        //pagingSourceFactory = { db.cocktailFtsDao().searchCocktails("*{$searchClause}*") }
    ).flow

    val searchIngredients = Pager(
        config = PagingConfig(SamogonApi.PAGING_SIZE),
        remoteMediator = ingredientsSearchRemoteMediator,
        pagingSourceFactory = { db.ingredientDao().searchIngredients(searchClause) }
    ).flow

    val filteredCocktails = Pager(
        config = PagingConfig(SamogonApi.PAGING_SIZE)) {
        FilterCocktailsPagingSource(api).apply {
            bases = this@SamogonRepo.bases
            tastes = this@SamogonRepo.tastes
            strengths = this@SamogonRepo.strengths
            difficulties = this@SamogonRepo.difficulties
        }
    }.flow

    val nameForCreate = createCocktailInteractor.name
    val descriptionForCreate = createCocktailInteractor.description
    val preparationTimeForCreate = createCocktailInteractor.time
    val imageForCreate = createCocktailInteractor.observeImage()
    val componentsForCreate = createCocktailInteractor.observeComponents()
    val volumeForCreate = createCocktailInteractor.volume
    val stepsForCreate = createCocktailInteractor.observeSteps()
    val stepNumberForCreate = createCocktailInteractor.stepNumber
    val errorsAtCreate = createCocktailInteractor.observeErrors()

    fun selectStrength(strength: Strength?) {
        createCocktailInteractor.selectStrength(strength)
    }
    fun selectDifficulty(difficulty: Difficulty?) {
        createCocktailInteractor.selectDifficulty(difficulty)
    }
    fun addComponentForCreate(component: CreateComponentRemote) {
        createCocktailInteractor.addComponent(component)
    }
    fun addStepForCreate(step: CreateStepRemote) {
        createCocktailInteractor.addStep(step)
    }
    fun addImageForCreate(image: File?) {
        createCocktailInteractor.addImage(image)
    }
    fun isCorrect() = createCocktailInteractor.isCorrect()

    fun clearCreation() {
        createCocktailInteractor.clear()
    }

    suspend fun createCocktail(imageUrl: String) = createCocktailInteractor.create(imageUrl)

    fun loadCocktail(id: String) = cocktailDataSource.load(id)

    fun loadSteps(id: String) = stepsDataSource.load(id)

    fun loadBases() = baseDataSource.load()

    fun loadDifficulties() = difficultiesDataSource.load()

    fun loadStrengths() = strengthsDataSource.load()

    fun loadTastes() = tasteDataSource.load()

    fun loadCompilations() = compilationsDataSource.load()

    fun loadComponents(id: String) = componentsDataSource.load(id)

    fun loadCocktailsForIngredient(id: String) = Pager(
        config = PagingConfig(SamogonApi.PAGING_SIZE)) {
        CocktailsForIngredientPagingSource(api, id)
    }.flow

    fun loadCocktailsForCompilation(id: String) = Pager(
        config = PagingConfig(SamogonApi.PAGING_SIZE)) {
        CocktailsForCompilationPagingSource(api, id)
    }.flow

}