package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class BasesDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load() = flow {
        val bases = db.baseDao().getAll()
        emit(bases)

        try {
            val updated = api.fetchBases()
            db.withTransaction {
                db.baseDao().insertAll(updated)
            }
            emit(db.baseDao().getAll())
        }catch (e: Exception){
            Log.e("BasesDataSource", e.message)
        }
    }
}