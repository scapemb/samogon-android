package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class CocktailDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load(id: String) = flow {
        try {
            val cached = db.cocktailDao().getCocktail(id)
            emit(cached)

            val cocktail = api.fetchCocktail(id)
            db.withTransaction {
                db.cocktailDao().insert(cocktail)
            }

            val updated = db.cocktailDao().getCocktail(id)
            emit(updated)
        }catch (e: Exception) {
            Log.e("CocktailDataSource", e.message)
        }
    }
}