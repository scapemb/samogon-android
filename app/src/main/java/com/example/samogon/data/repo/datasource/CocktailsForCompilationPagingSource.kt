package com.example.samogon.data.repo.datasource

import androidx.paging.PagingSource
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.remote.api.SamogonApi
import java.lang.Exception

class CocktailsForCompilationPagingSource(private val api: SamogonApi, val compilationId: String) : PagingSource<Int, Cocktail>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Cocktail> {
        return try {
            val offset = params.key ?: 0
            val response = api.fetchCocktailsForCompilation(compilationId, offset = offset)
            LoadResult.Page(
                data = response,
                prevKey = null,
                nextKey = when(response.isEmpty() || response.size < SamogonApi.PAGING_SIZE) {
                    true -> null
                    false -> offset + SamogonApi.PAGING_SIZE
                })
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}