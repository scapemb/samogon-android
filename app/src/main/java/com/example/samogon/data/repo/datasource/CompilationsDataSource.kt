package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Compilation
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.local.model.crossref.CompilationCocktailCrossRef
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class CompilationsDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load() = flow {
        val compilations = db.compilationDao().getCompilationsWithCocktails()
        emit(compilations)

        try {
            val updated = api.fetchCompilations()
            db.withTransaction {
                db.compilationDao().deleteAll()
                updated.forEach { compilation ->
                    db.compilationDao().insert(Compilation(compilation))
                    db.cocktailDao().insertAll(compilation.cocktails)
                    compilation.cocktails.forEach { cocktail ->
                        db.compilationDao().insertWithCocktails(CompilationCocktailCrossRef(compilation.compilationId, cocktail.cocktailId))
                    }
                }
            }
            emit(db.compilationDao().getCompilationsWithCocktails())

        }catch (e: Exception){
            Log.e("CompilationsDataSource", e.message)
        }
    }
}