package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.crossref.CocktailComponentCrossRef
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class ComponentsDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load(cocktailId: String) = flow {
        val cocktailWithComponents = db.cocktailDao().getCocktailWithComponents(cocktailId)
        emit(cocktailWithComponents)

        try {
            val cocktail = api.fetchCocktail(cocktailId)
            val components = api.fetchComponents(cocktailId)
            with(db) {
                withTransaction {
                    componentDao().insertAll(components)
                    cocktailDao().insert(cocktail)
                    components.forEach {
                        cocktailDao().insertWithComponents(
                            CocktailComponentCrossRef(cocktailId, it.componentId)
                        )
                    }
                }
            }

            val updated = db.cocktailDao().getCocktailWithComponents(cocktailId)
            emit(updated)
        }catch (e: Exception) {
            Log.e("ComponentsDataSource", e.message)
        }
    }
}