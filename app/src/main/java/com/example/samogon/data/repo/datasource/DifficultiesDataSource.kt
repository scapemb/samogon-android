package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class DifficultiesDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load() = flow {
        val difficulties = db.difficultyDao().getAll()
        emit(difficulties)

        try {
            val updated = api.fetchDifficulties()
            db.withTransaction {
                db.difficultyDao().insertAll(updated)
            }
            emit(db.difficultyDao().getAll())
        }catch (e: Exception){
            Log.e("DifficultiesDataSource", e.message)
        }
    }
}