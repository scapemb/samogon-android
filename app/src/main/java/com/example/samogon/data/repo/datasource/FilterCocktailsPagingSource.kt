package com.example.samogon.data.repo.datasource

import androidx.paging.PagingSource
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.remote.api.SamogonApi
import java.lang.Exception

class FilterCocktailsPagingSource(private val api: SamogonApi) : PagingSource<Int, Cocktail>() {
    var bases: List<String>? = null
    var tastes: List<String>? = null
    var strengths: List<String>? = null
    var difficulties: List<String>? = null
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Cocktail> {
        return try {
            val offset = params.key ?: 0
            if(bases.isNullOrEmpty()) bases = null
            if(tastes.isNullOrEmpty()) tastes = null
            if(strengths.isNullOrEmpty()) strengths = null
            if(difficulties.isNullOrEmpty()) difficulties = null

            val response = api.filterCocktails(bases, tastes, difficulties, strengths, offset = offset)

            LoadResult.Page(
                data = response,
                prevKey = null,
                nextKey = when(response.isEmpty() || response.size < SamogonApi.PAGING_SIZE) {
                    true -> null
                    false -> offset + SamogonApi.PAGING_SIZE
                })
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}