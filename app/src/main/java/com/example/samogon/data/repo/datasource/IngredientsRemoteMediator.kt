package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.data.remote.api.SamogonApi
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_OFFSET = 0
private const val TAG = "IngredientsRemoteMdtr"

@OptIn(ExperimentalPagingApi::class)
class IngredientsRemoteMediator(
    private val api: SamogonApi,
    private val db: AppDatabase,
    private val isSearchMode: Boolean = false
): RemoteMediator<Int, Ingredient>() {

    var searchClause: String? = null

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, Ingredient>
    ): MediatorResult {
        if(isSearchMode && searchClause.isNullOrEmpty()) {
            Log.d(TAG,"Empty clause")
            return MediatorResult.Success(endOfPaginationReached = true)
        }
        val offset = when(loadType) {
            LoadType.REFRESH -> {
                Log.d(TAG,"LoadType.REFRESH")
                STARTING_OFFSET
            }
            LoadType.PREPEND -> {
                Log.d(TAG,"LoadType.PREPEND")
                return MediatorResult.Success(endOfPaginationReached = true)
            }
            LoadType.APPEND -> {
                Log.d(TAG,"LoadType.APPEND")
                db.withTransaction {
                    when(isSearchMode) {
                        true ->  db.ingredientDao().getCount(searchClause!!)
                        false ->  db.ingredientDao().getCount()
                    } ?: STARTING_OFFSET
                }
            }
        }

        try {
            Log.d(TAG,"fetching with offset $offset")
            val ingredients = when(isSearchMode) {
                    true -> api.searchIngredients(searchClause = searchClause!!, offset = offset)
                    false -> api.fetchIngredients(offset = offset)
                }

            val endIsReached = ingredients.isEmpty()
            db.withTransaction {
                if(loadType == LoadType.REFRESH && !isSearchMode) {
                    db.ingredientDao().deleteAll()
                }
                db.ingredientDao().insertAll(ingredients)
            }
            return MediatorResult.Success(endOfPaginationReached = endIsReached)
        } catch (exception: IOException) {
            Log.e(TAG,"IOException")
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            Log.e(TAG,"HttpException")
            return MediatorResult.Error(exception)
        }
    }
}