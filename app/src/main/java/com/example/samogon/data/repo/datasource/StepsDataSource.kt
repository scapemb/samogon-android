package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class StepsDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load(cocktailId: String) = flow {
        val cocktailWithSteps = db.cocktailDao().getCocktailWithSteps(cocktailId)
        emit(cocktailWithSteps)

        try {
            val cocktail = api.fetchCocktail(cocktailId)
            val steps = api.fetchSteps(cocktailId)
            db.withTransaction {
                db.stepDao().insertAll(steps)
                db.cocktailDao().insert(cocktail)
                steps.forEach {
                    db.cocktailDao().insertWithSteps(CocktailStepCrossRef(cocktailId, it.stepId))
                }
            }

            val updated = db.cocktailDao().getCocktailWithSteps(cocktailId)
            emit(updated)
        }catch (e: Exception){
            Log.e("StepsDataSource", e.message)
        }
    }
}