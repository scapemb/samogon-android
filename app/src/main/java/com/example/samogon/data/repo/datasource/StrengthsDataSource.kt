package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.crossref.CocktailStepCrossRef
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class StrengthsDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load() = flow {
        val strengths = db.strengthDao().getAll()
        emit(strengths)

        try {
            val updated = api.fetchStrengths()
            db.withTransaction {
                db.strengthDao().insertAll(updated)
            }
            emit(db.strengthDao().getAll())
        }catch (e: Exception){
            Log.e("StrengthsDataSource", e.message)
        }
    }
}