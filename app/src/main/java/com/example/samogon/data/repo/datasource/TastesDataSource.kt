package com.example.samogon.data.repo.datasource

import android.util.Log
import androidx.room.withTransaction
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.remote.api.SamogonApi
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class TastesDataSource(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    fun load() = flow {
        val tastes = db.tasteDao().getAll()
        emit(tastes)

        try {
            val updated = api.fetchTastes()
            db.withTransaction {
                db.tasteDao().insertAll(updated)
            }
            emit(db.tasteDao().getAll())
        }catch (e: Exception){
            Log.e("TastesDataSource", e.message)
        }
    }
}