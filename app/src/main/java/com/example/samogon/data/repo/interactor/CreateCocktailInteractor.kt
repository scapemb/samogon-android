package com.example.samogon.data.repo.interactor

import androidx.lifecycle.*
import com.backendless.Backendless
import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Difficulty
import com.example.samogon.data.local.model.Strength
import com.example.samogon.data.remote.api.SamogonApi
import com.example.samogon.data.remote.model.CreateCocktailFullRemote
import com.example.samogon.data.remote.model.CreateCocktailRemote
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.data.remote.model.CreateStepRemote
import com.example.samogon.utils.enums.CreateCocktailErrorType
import com.example.samogon.utils.extension.MathExtensions
import kotlinx.coroutines.flow.flow
import java.io.File

class CreateCocktailInteractor(
    private val api: SamogonApi,
    private val db: AppDatabase
) {
    val name: MutableLiveData<String> = MutableLiveData()
    val description: MutableLiveData<String> = MutableLiveData()
    val time: MutableLiveData<String> = MutableLiveData()
    val strength: MutableLiveData<Strength> = MutableLiveData()
    val difficulty: MutableLiveData<Difficulty> = MutableLiveData()
    val image: MutableLiveData<File?> = MutableLiveData()
    val components: MutableLiveData<MutableList<CreateComponentRemote>> = MutableLiveData(
        mutableListOf())
    val steps: MutableLiveData<MutableList<CreateStepRemote>> = MutableLiveData(mutableListOf())
    val volume: MutableLiveData<Int> = MutableLiveData(0)
    val stepNumber: MutableLiveData<Int> = MutableLiveData(0)

    val errors: MutableLiveData<CreateCocktailErrorType> = MutableLiveData()

    fun observeName(): LiveData<String> {
        val name = MediatorLiveData<String>()
        name.addSource(this.name) {
            name.value = it
        }
        return name
    }
    fun observeDescription(): LiveData<String> {
        val description = MediatorLiveData<String>()
        description.addSource(this.description) {
            description.value = it
        }
        return description
    }
    fun observeImage(): LiveData<File> {
        val image = MediatorLiveData<File>()
        image.addSource(this.image) {
            image.value = it
        }
        return image
    }
    fun observeComponents(): LiveData<MutableList<CreateComponentRemote>> {
        val components = MediatorLiveData<MutableList<CreateComponentRemote>>()
        components.addSource(this.components) {
            components.value = it
            volume.value = it.sumBy { it.volume }
        }
        return components
    }
    fun observeSteps(): LiveData<MutableList<CreateStepRemote>> {
        val steps = MediatorLiveData<MutableList<CreateStepRemote>>()
        steps.addSource(this.steps) {
            steps.value = it
            stepNumber.value = it.size
        }
        return steps
    }
    fun observeErrors(): LiveData<CreateCocktailErrorType> {
        val errors = MediatorLiveData<CreateCocktailErrorType>()
        errors.addSource(this.errors) {
            errors.value = it
        }
        return errors
    }

    fun selectStrength(strength: Strength?) {
        if (strength == null) return
        this.strength.value = strength
    }
    fun selectDifficulty(difficulty: Difficulty?) {
        if (difficulty == null) return
        this.difficulty.value = difficulty
    }

    fun addImage(image: File?) {
        if (image == null) return
        this.image.value = image
    }

    fun addComponent(component: CreateComponentRemote?) {
        if (component == null) return
        components.value?.let {
            it.add(component)
            components.value = it
        }
        updateComponentParts()
    }

    fun addStep(step: CreateStepRemote?) {
        if (step == null) return
        steps.value?.let {
            it.add(step)
            steps.value = it
        }
    }

    fun isCorrect(): Boolean {
        val name = name.value
        val description = description.value
        val time = time.value
        val components = components.value
        val steps = steps.value

        if(name.isNullOrEmpty()) {
            sendError(CreateCocktailErrorType.EMPTY_NAME)
            return false
        } else if(name.length > 250) {
            sendError(CreateCocktailErrorType.TOO_LONG_NAME)
            return false
        }
        if(description.isNullOrEmpty()) {
            sendError(CreateCocktailErrorType.EMPTY_DESCRIPTION)
            return false
        } else if(description.length > 250) {
            sendError(CreateCocktailErrorType.TOO_LONG_DESCRIPTION)
            return false
        }
        if(time.isNullOrEmpty()) {
            sendError(CreateCocktailErrorType.EMPTY_TIME)
            return false
        }
        if(components.isNullOrEmpty()) {
            sendError(CreateCocktailErrorType.EMPTY_COMPONENTS)
            return false
        }
        if(steps.isNullOrEmpty()) {
            sendError(CreateCocktailErrorType.EMPTY_STEPS)
            return false
        }
        sendError(CreateCocktailErrorType.NONE)
        return true
    }

    fun clear() {
        name.postValue("")
        description.postValue("")
        time.postValue("")
        volume.postValue(0)
        components.postValue(arrayListOf())
        steps.postValue(arrayListOf())
        image.postValue(null)
    }

    suspend fun create(imageUrl: String): Cocktail?{
        updateComponentParts()

        val name = name.value ?: ""
        val description = description.value ?: ""
        val time = time.value ?: "0"
        val strength = strength.value
        val volume = volume.value ?: 0
        val difficulty = difficulty.value
        val components = components.value
        val steps = steps.value

        if( isCorrect() && strength != null && difficulty != null && components != null && steps != null) {
            val cocktail = CreateCocktailRemote(
                name = name,
                description = description,
                preparationTime = Integer.parseInt(time),
                image = imageUrl,
                volume = volume,
                difficultyId = difficulty.difficultyId,
                strengthId = strength.strengthId
                )

            val fullCocktail = CreateCocktailFullRemote(
                cocktail, components, steps
            )

            val new = api.createCocktail(fullCocktail)
            db.cocktailDao().insert(new)
            return db.cocktailDao().getCocktail(new.cocktailId)
        }
        return null
    }

    fun sendError(type: CreateCocktailErrorType) {
        errors.postValue(type)
    }

    fun updateComponentParts() {
        val components = components.value
        components?.let {
            val liquidItems =  it.filter { it.volume > 0 }
            val gcd = MathExtensions.gcd(liquidItems.map { it.volume })

            it.forEach { component ->
                component.value = component.volume / gcd
                component.valueType = 1
            }

            this.components.postValue(it)
        }
    }
}