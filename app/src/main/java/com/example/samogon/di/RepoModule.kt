package com.example.samogon.di

import com.example.samogon.data.local.db.AppDatabase
import com.example.samogon.data.remote.api.SamogonApi
import com.example.samogon.data.repo.FileRepo
import com.example.samogon.data.repo.SamogonRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.hilt.android.scopes.ServiceScoped
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepoModule {

    @Provides
    @Singleton
    fun provideSamogonRepo(
        api: SamogonApi,
        db: AppDatabase
    ): SamogonRepo = SamogonRepo(api, db)

    @Provides
    @Singleton
    fun provideFileRepo(
        api: SamogonApi,
        db: AppDatabase
    ): FileRepo = FileRepo(api, db)
}