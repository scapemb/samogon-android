package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.samogon.data.local.model.*
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.ui.viewholder.*

class BaseAdapter(val vm: SearchVM): ListAdapter<Base, BaseViewHolder>(BASE_COMPARATOR) {
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder = BaseViewHolder.create(parent, vm)

    companion object {
        private val BASE_COMPARATOR = object : DiffUtil.ItemCallback<Base>() {
            override fun areItemsTheSame(oldItem: Base, newItem: Base): Boolean {
                return oldItem.baseId == newItem.baseId
            }

            override fun areContentsTheSame(oldItem: Base, newItem: Base): Boolean =
                oldItem.name == newItem.name && oldItem.isSelected == newItem.isSelected
        }
    }
}