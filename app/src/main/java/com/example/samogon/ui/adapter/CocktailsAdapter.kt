package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.ui.viewholder.CocktailViewHolder
import com.example.samogon.utils.listener.CocktailClickListener

class CocktailsAdapter(val listener: CocktailClickListener): PagingDataAdapter<Cocktail, CocktailViewHolder>(COCKTAIL_COMPARATOR) {
    override fun onBindViewHolder(holder: CocktailViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CocktailViewHolder = CocktailViewHolder.create(parent, listener)

    companion object {
        private val COCKTAIL_COMPARATOR = object : DiffUtil.ItemCallback<Cocktail>() {
            override fun areItemsTheSame(oldItem: Cocktail, newItem: Cocktail): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Cocktail, newItem: Cocktail): Boolean =
                oldItem.name == newItem.name && oldItem.description == newItem.description
        }
    }
}