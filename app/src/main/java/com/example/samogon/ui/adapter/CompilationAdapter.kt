package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.CompilationWithCocktails
import com.example.samogon.data.local.model.Difficulty
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.ui.viewholder.CompilationCocktailViewHolder
import com.example.samogon.ui.viewholder.CompilationViewHolder
import com.example.samogon.ui.viewholder.DifficultyViewHolder
import com.example.samogon.utils.listener.CocktailClickListener

class CompilationAdapter(val cocktailListener: CocktailClickListener): ListAdapter<Cocktail, CompilationCocktailViewHolder>(COCKTAIL_COMPARATOR) {
    override fun onBindViewHolder(holder: CompilationCocktailViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompilationCocktailViewHolder = CompilationCocktailViewHolder.create(parent, cocktailListener)

    companion object {
        private val COCKTAIL_COMPARATOR = object : DiffUtil.ItemCallback<Cocktail>() {
            override fun areItemsTheSame(oldItem: Cocktail, newItem: Cocktail): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Cocktail, newItem: Cocktail): Boolean =
                oldItem.name == newItem.name && oldItem.description == newItem.description
        }
    }

}