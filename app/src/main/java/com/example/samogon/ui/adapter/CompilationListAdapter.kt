package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.samogon.data.local.model.CompilationWithCocktails
import com.example.samogon.data.local.model.Difficulty
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.ui.viewholder.CompilationViewHolder
import com.example.samogon.ui.viewholder.DifficultyViewHolder
import com.example.samogon.utils.listener.CocktailClickListener
import com.example.samogon.utils.listener.CompilationClickListener
import com.example.samogon.utils.listener.OnClickListener

class CompilationListAdapter(val seeAllListener: CompilationClickListener, val cocktailListener: CocktailClickListener): ListAdapter<CompilationWithCocktails, CompilationViewHolder>(COMPILATION_COMPARATOR) {
    override fun onBindViewHolder(holder: CompilationViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompilationViewHolder = CompilationViewHolder.create(parent, seeAllListener, cocktailListener)

    companion object {
        private val COMPILATION_COMPARATOR = object : DiffUtil.ItemCallback<CompilationWithCocktails>() {
            override fun areItemsTheSame(oldItem: CompilationWithCocktails, newItem: CompilationWithCocktails): Boolean {
                return oldItem.compilation.compilationId == newItem.compilation.compilationId
            }

            override fun areContentsTheSame(oldItem: CompilationWithCocktails, newItem: CompilationWithCocktails): Boolean =
                oldItem.compilation.name == newItem.compilation.name && oldItem.cocktails.size == newItem.cocktails.size
        }
    }
}