package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.CocktailWithTastes
import com.example.samogon.data.local.model.Component
import com.example.samogon.data.local.model.Step
import com.example.samogon.ui.viewholder.CocktailViewHolder
import com.example.samogon.ui.viewholder.ComponentClickListener
import com.example.samogon.ui.viewholder.ComponentViewHolder
import com.example.samogon.ui.viewholder.StepViewHolder

class ComponentsAdapter(val listener: ComponentClickListener): ListAdapter<Component, ComponentViewHolder>(COMPONENT_COMPARATOR) {
    var cocktailVolume: Int = 0
    var cocktailParts: Int = 0

    override fun onBindViewHolder(holder: ComponentViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentViewHolder = ComponentViewHolder.create(this, parent, listener)

    companion object {
        private val COMPONENT_COMPARATOR = object : DiffUtil.ItemCallback<Component>() {
            override fun areItemsTheSame(oldItem: Component, newItem: Component): Boolean {
                return oldItem.componentId == newItem.componentId
            }

            override fun areContentsTheSame(oldItem: Component, newItem: Component): Boolean =
                oldItem.componentId == newItem.componentId
        }
    }
}