package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.ui.viewholder.AddItemViewHolder
import com.example.samogon.ui.viewholder.ComponentForCreateViewHolder
import com.example.samogon.utils.listener.OnClickListener

class ComponentsForCreateAdapter(val addItemClickListener: OnClickListener): ListAdapter<CreateComponentRemote, RecyclerView.ViewHolder>(COMPONENT_COMPARATOR) {
    private val FOOTER_VIEW = 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is ComponentForCreateViewHolder) {
            return holder.bind(getItem(position))
        }
        if(holder is AddItemViewHolder) {
            return holder.bind()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when(viewType){
        FOOTER_VIEW -> AddItemViewHolder.create(parent, addItemClickListener)
        else -> ComponentForCreateViewHolder.create(parent)
    }

    override fun getItemViewType(position: Int): Int {
        if(position == this.currentList.size) {
            return FOOTER_VIEW
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int = this.currentList.size + 1

    companion object {
        private val COMPONENT_COMPARATOR = object : DiffUtil.ItemCallback<CreateComponentRemote>() {
            override fun areItemsTheSame(oldItem: CreateComponentRemote, newItem: CreateComponentRemote): Boolean {
                return oldItem.ingredientId == newItem.ingredientId
            }

            override fun areContentsTheSame(oldItem: CreateComponentRemote, newItem: CreateComponentRemote): Boolean =
                oldItem.ingredientId == newItem.ingredientId && oldItem.value == newItem.value && oldItem.volume == newItem.volume
        }
    }
}