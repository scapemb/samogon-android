package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.samogon.data.local.model.*
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.ui.viewholder.*

class DifficultyAdapter(val vm: SearchVM): ListAdapter<Difficulty, DifficultyViewHolder>(DIFFICULTY_COMPARATOR) {
    override fun onBindViewHolder(holder: DifficultyViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DifficultyViewHolder = DifficultyViewHolder.create(parent, vm)

    companion object {
        private val DIFFICULTY_COMPARATOR = object : DiffUtil.ItemCallback<Difficulty>() {
            override fun areItemsTheSame(oldItem: Difficulty, newItem: Difficulty): Boolean {
                return oldItem.difficultyId == newItem.difficultyId
            }

            override fun areContentsTheSame(oldItem: Difficulty, newItem: Difficulty): Boolean =
                oldItem.name == newItem.name && oldItem.isSelected == newItem.isSelected
        }
    }
}