package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.ui.viewholder.IngredientClickListener
import com.example.samogon.ui.viewholder.IngredientViewHolder

class IngredientsAdapter(val listener: IngredientClickListener): PagingDataAdapter<Ingredient, IngredientViewHolder>(INGREDIENT_COMPARATOR) {
    override fun onBindViewHolder(holder: IngredientViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientViewHolder = IngredientViewHolder.create(parent, listener)

    companion object {
        private val INGREDIENT_COMPARATOR = object : DiffUtil.ItemCallback<Ingredient>() {
            override fun areItemsTheSame(oldItem: Ingredient, newItem: Ingredient): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Ingredient, newItem: Ingredient): Boolean =
                oldItem.name == newItem.name && oldItem.type == newItem.type
        }
    }
}