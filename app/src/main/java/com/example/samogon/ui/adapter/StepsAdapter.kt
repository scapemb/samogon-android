package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.CocktailWithTastes
import com.example.samogon.data.local.model.Step
import com.example.samogon.ui.viewholder.CocktailViewHolder
import com.example.samogon.ui.viewholder.StepViewHolder

class StepsAdapter: ListAdapter<Step, StepViewHolder>(STEP_COMPARATOR) {
    override fun onBindViewHolder(holder: StepViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StepViewHolder = StepViewHolder.create(parent)

    companion object {
        private val STEP_COMPARATOR = object : DiffUtil.ItemCallback<Step>() {
            override fun areItemsTheSame(oldItem: Step, newItem: Step): Boolean {
                return oldItem.number == newItem.number
            }

            override fun areContentsTheSame(oldItem: Step, newItem: Step): Boolean =
                oldItem.number == newItem.number && oldItem.description == newItem.description
        }
    }
}