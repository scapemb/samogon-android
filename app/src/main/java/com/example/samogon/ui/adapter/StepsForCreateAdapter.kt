package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.data.remote.model.CreateStepRemote
import com.example.samogon.ui.viewholder.AddItemViewHolder
import com.example.samogon.ui.viewholder.ComponentForCreateViewHolder
import com.example.samogon.ui.viewholder.StepForCreateViewHolder
import com.example.samogon.utils.listener.OnClickListener

class StepsForCreateAdapter(val addItemClickListener: OnClickListener): ListAdapter<CreateStepRemote, RecyclerView.ViewHolder>(STEP_COMPARATOR) {
    private val FOOTER_VIEW = 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is StepForCreateViewHolder) {
            return holder.bind(getItem(position))
        }
        if(holder is AddItemViewHolder) {
            return holder.bind()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when(viewType){
        FOOTER_VIEW -> AddItemViewHolder.create(parent, addItemClickListener)
        else -> StepForCreateViewHolder.create(parent)
    }

    override fun getItemViewType(position: Int): Int {
        if(position == this.currentList.size) {
            return FOOTER_VIEW
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int = this.currentList.size + 1

    companion object {
        private val STEP_COMPARATOR = object : DiffUtil.ItemCallback<CreateStepRemote>() {
            override fun areItemsTheSame(oldItem: CreateStepRemote, newItem: CreateStepRemote): Boolean {
                return oldItem.description == newItem.description
            }

            override fun areContentsTheSame(oldItem: CreateStepRemote, newItem: CreateStepRemote): Boolean =
                oldItem.description == newItem.description && oldItem.number == newItem.number
        }
    }
}