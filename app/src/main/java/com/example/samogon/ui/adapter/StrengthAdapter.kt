package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.samogon.data.local.model.*
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.ui.viewholder.*

class StrengthAdapter(val vm: SearchVM): ListAdapter<Strength, StrengthViewHolder>(STRENGTH_COMPARATOR) {
    override fun onBindViewHolder(holder: StrengthViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StrengthViewHolder = StrengthViewHolder.create(parent, vm)

    companion object {
        private val STRENGTH_COMPARATOR = object : DiffUtil.ItemCallback<Strength>() {
            override fun areItemsTheSame(oldItem: Strength, newItem: Strength): Boolean {
                return oldItem.strengthId == newItem.strengthId
            }

            override fun areContentsTheSame(oldItem: Strength, newItem: Strength): Boolean =
                oldItem.name == newItem.name && oldItem.isSelected == newItem.isSelected
        }
    }
}