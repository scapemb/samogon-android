package com.example.samogon.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.samogon.data.local.model.*
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.ui.viewholder.*

class TasteAdapter(val vm: SearchVM? = null): ListAdapter<Taste, TasteViewHolder>(TASTE_COMPARATOR) {
    override fun onBindViewHolder(holder: TasteViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TasteViewHolder = TasteViewHolder.create(parent, vm)

    companion object {
        private val TASTE_COMPARATOR = object : DiffUtil.ItemCallback<Taste>() {
            override fun areItemsTheSame(oldItem: Taste, newItem: Taste): Boolean {
                return oldItem.tasteId == newItem.tasteId
            }

            override fun areContentsTheSame(oldItem: Taste, newItem: Taste): Boolean =
                oldItem.name == newItem.name && oldItem.isSelected == newItem.isSelected
        }
    }
}