package com.example.samogon.ui.cocktail

import android.content.Intent
import android.os.Bundle
import android.transition.ChangeBounds
import android.transition.TransitionInflater
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.Scene
import com.example.samogon.R
import com.example.samogon.data.local.model.Component
import com.example.samogon.databinding.FragmentCocktailBinding
import com.example.samogon.ui.adapter.ComponentsAdapter
import com.example.samogon.ui.adapter.StepsAdapter
import com.example.samogon.ui.cocktails.CocktailListFragmentDirections
import com.example.samogon.ui.viewholder.ComponentClickListener
import com.example.samogon.utils.enums.CocktailListType
import com.example.samogon.utils.service.CreateCocktailService
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_cocktail.*
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class CocktailFragment : Fragment(), ComponentClickListener {

    private val vm by viewModels<CocktailVM>()
    private val args: CocktailFragmentArgs by navArgs()

    private val stepsAdapter = StepsAdapter()
    private val componentsAdapter = ComponentsAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentCocktailBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_cocktail, container, false
        )
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        sharedElementReturnTransition =  TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        binding.apply {
            lifecycleOwner = this@CocktailFragment
            vm = this@CocktailFragment.vm
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initStepAdapter()
        initComponentAdapter()
        subscribeToVm()
        handleArgs()
    }

    private fun subscribeToVm() {
        vm.apply {
            observeCocktailAvailability().observe(viewLifecycleOwner, Observer {
            })
            observeCocktail().observe(viewLifecycleOwner, Observer {
                componentsAdapter.cocktailVolume = it.volume
                componentsAdapter.notifyDataSetChanged()
            })
            observeSteps().observe(viewLifecycleOwner, Observer {
                stepsAdapter.submitList(it)
            })
            observeComponents().observe(viewLifecycleOwner, Observer {
                val componentsVolumeParts = it.sumBy { it.value }
                componentsAdapter.cocktailParts = componentsVolumeParts
                componentsAdapter.submitList(it)
            })
        }
    }

    private fun handleArgs() {
        vm.updateCocktailId(args.cocktailId)
        if(args.isFromNotification) {
            val intent = Intent(requireContext(), CreateCocktailService::class.java).apply {
                action = CreateCocktailService.ACTION_STOP
            }
            requireContext().startService(intent)
        }
    }

    private fun initStepAdapter() {
        steps.adapter = stepsAdapter
    }

    private fun initComponentAdapter() {
        with(components) {
            adapter = componentsAdapter
            layoutManager = FlexboxLayoutManager(context).apply {
                flexDirection = FlexDirection.ROW
                flexWrap = FlexWrap.WRAP
                justifyContent = JustifyContent.FLEX_END
            }
        }
    }

    override fun onClick(component: Component) {
        val action = CocktailFragmentDirections.actionCocktailFragmentToCocktailListFragment(component.ingredient.ingredientId, CocktailListType.COCKTAILS_FOR_INGREDIENT)
        findNavController().navigate(action)
    }
}