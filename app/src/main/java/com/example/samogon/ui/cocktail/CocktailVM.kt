package com.example.samogon.ui.cocktail

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Component
import com.example.samogon.data.local.model.Step
import com.example.samogon.data.repo.SamogonRepo
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class CocktailVM @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {

    val cocktailId: MutableLiveData<String> = MutableLiveData()
    val cocktail: MutableLiveData<Cocktail> = MutableLiveData()
    val steps: MutableLiveData<List<Step>> = MutableLiveData()
    val components: MutableLiveData<List<Component>> = MutableLiveData()

    fun observeCocktailAvailability(): LiveData<String> {
        val id = MediatorLiveData<String>()
        id.addSource(cocktailId) {
            id.value = it
            load(it)
            loadSteps(it)
            loadComponents(it)
        }
        return id
    }

    fun observeCocktail(): LiveData<Cocktail> {
        val cocktail = MediatorLiveData<Cocktail>()
        cocktail.addSource(this.cocktail) {
            cocktail.value = it
        }
        return cocktail
    }

    fun observeSteps(): LiveData<List<Step>> {
        val steps = MediatorLiveData<List<Step>>()
        steps.addSource(this.steps) {
            steps.value = it
        }
        return steps
    }

    fun observeComponents(): LiveData<List<Component>> {
        val components = MediatorLiveData<List<Component>>()
        components.addSource(this.components) {
            components.value = it
        }
        return components
    }

    fun updateCocktailId(new: String) {
        cocktailId.value = new
    }

    private fun load(id: String) = viewModelScope.launch {
        repo.loadCocktail(id).collect {
            cocktail.value = it
        }
    }

    private fun loadSteps(id: String) = viewModelScope.launch {
        repo.loadSteps(id).collectLatest {
            it?.let {
                steps.value = it.steps.sortedBy { it.number }
            }
        }
    }

    private fun loadComponents(id: String) = viewModelScope.launch {
        repo.loadComponents(id).collectLatest {
            it?.let {
                components.value = it.components
            }
        }
    }
}