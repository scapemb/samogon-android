package com.example.samogon.ui.cocktails

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.databinding.FragmentListCocktailsBinding
import com.example.samogon.ui.adapter.CocktailsAdapter
import com.example.samogon.utils.enums.CocktailListType
import com.example.samogon.utils.listener.CocktailClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_list_cocktails.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter


@AndroidEntryPoint
class CocktailListFragment : Fragment(), CocktailClickListener {

    private val vm by viewModels<CocktailListVM>()
    private val args: CocktailListFragmentArgs by navArgs()

    private val adapter = CocktailsAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentListCocktailsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_list_cocktails, container, false
        )
        binding.apply {
            lifecycleOwner = this@CocktailListFragment
            vm = this@CocktailListFragment.vm
        }
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_cocktail_list, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initSwipeToRefresh()
    }

    private fun initAdapter() {
        list.adapter = adapter
        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collectLatest { loadStates ->
                swipe_refresh?.isRefreshing = loadStates.refresh == LoadState.Loading
            }
        }
        lifecycleScope.launchWhenCreated {
            when(args.type) {
                CocktailListType.COCKTAILS_FOR_INGREDIENT -> {
                    args.ingredientId?.let {
                        vm.getCocktailsForIngredient(it).collectLatest {
                            adapter.submitData(it)
                        }
                    }
                }
                CocktailListType.COCKTAILS_FOR_COMPILATION -> {
                    args.compilationId?.let {
                        vm.getCocktailsForCompilation(it).collectLatest {
                            adapter.submitData(it)
                        }
                    }
                }
                CocktailListType.COCKTAILS_ALL -> {
                    vm.cocktails.collectLatest {
                        adapter.submitData(it)
                    }
                }
            }
        }
        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow
                .distinctUntilChangedBy { it.refresh }
                .filter { it.refresh is LoadState.NotLoading }
        }
    }

    private fun initSwipeToRefresh() {
        swipe_refresh?.setOnRefreshListener { adapter.refresh() }
    }

    override fun onClick(cocktail: Cocktail) {
        val action = CocktailListFragmentDirections.actionCocktailListFragmentToCocktailFragment(cocktail.cocktailId)
        //val extras = FragmentNavigatorExtras(iv to "cocktailImage", name to "cocktailName")
        findNavController().navigate(action)
    }
}