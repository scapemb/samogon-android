package com.example.samogon.ui.cocktails

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.samogon.data.repo.SamogonRepo
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class CocktailListVM @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {

    val cocktails = repo.cocktails
        .cachedIn(viewModelScope)

    fun getCocktailsForIngredient(ingredientId: String) = repo.loadCocktailsForIngredient(ingredientId)
            .cachedIn(viewModelScope)

    fun getCocktailsForCompilation(compilationId: String) = repo.loadCocktailsForCompilation(compilationId)
            .cachedIn(viewModelScope)
}