package com.example.samogon.ui.compilations

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Compilation
import com.example.samogon.databinding.FragmentListCompilationsBinding
import com.example.samogon.ui.adapter.CompilationListAdapter
import com.example.samogon.utils.enums.CocktailListType
import com.example.samogon.utils.listener.CocktailClickListener
import com.example.samogon.utils.listener.CompilationClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_list_cocktails.*
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class CompilationListFragment : Fragment(), CompilationClickListener, CocktailClickListener {

    private val vm by viewModels<CompilationListVM>()

    private val adapter = CompilationListAdapter(this, this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentListCompilationsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_list_compilations, container, false
        )
        binding.apply {
            lifecycleOwner = this@CompilationListFragment
            vm = this@CompilationListFragment.vm
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    private fun initAdapter() {
        list.adapter = adapter
        lifecycleScope.launchWhenCreated {
            vm.compilations.collectLatest {
                adapter.submitList(it)
            }
        }
    }

    override fun onClick(cocktail: Cocktail) {
        val action = CompilationListFragmentDirections.actionCompilationListFragmentToCocktailFragment(cocktail.cocktailId)
        findNavController().navigate(action)
    }

    override fun onClick(compilation: Compilation) {
        val action = CompilationListFragmentDirections.actionCompilationListFragmentToCocktailListFragment(compilationId = compilation.compilationId, type = CocktailListType.COCKTAILS_FOR_COMPILATION)
        findNavController().navigate(action)
    }
}