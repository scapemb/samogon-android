package com.example.samogon.ui.compilations

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.samogon.data.repo.SamogonRepo
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class CompilationListVM @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {

    val compilations = repo.loadCompilations()

}