package com.example.samogon.ui.create

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.samogon.R
import com.example.samogon.databinding.FragmentCreateBinding
import com.example.samogon.ui.adapter.ComponentsForCreateAdapter
import com.example.samogon.ui.adapter.StepsForCreateAdapter
import com.example.samogon.ui.dialogs.createComponent.CreateComponentDialog
import com.example.samogon.ui.dialogs.createStep.CreateStepDialog
import com.example.samogon.utils.listener.OnClickListener
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_create.*
import kotlinx.coroutines.flow.collectLatest
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.File


@AndroidEntryPoint
class CreateFragment : Fragment() {
    private val vm by viewModels<CreateVM>()

    val PICK_IMAGE_REQUEST_CODE = 101
    companion object {
        final const val CAMERA_AND_STORAGE = 1300
    }

    val addComponentListener = object : OnClickListener {
        override fun onClick() {
            createComponent()
        }
    }
    val addStepListener = object : OnClickListener {
        override fun onClick() {
            createStep()
        }
    }

    private val componentsAdapter = ComponentsForCreateAdapter(addComponentListener)
    private val stepsAdapter = StepsForCreateAdapter(addStepListener)
    lateinit var strengthAdapter: ArrayAdapter<String>
    lateinit var difficultyAdapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentCreateBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_create, container, false
        )

        binding.apply {
            lifecycleOwner = this@CreateFragment
            vm = this@CreateFragment.vm
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initComponentsAdapter()
        initStepsAdapter()
        initStrengthAdapter()
        initDifficultyAdapter()
        subscribeToVm()
    }

    private fun subscribeToVm() {
        vm.stepNumber.observe(viewLifecycleOwner, Observer {
        })
        vm.observeErrors().observe(viewLifecycleOwner, Observer {
        })
        vm.addPhotoEvent.observe(viewLifecycleOwner, Observer {
            pickImage()
        })
    }

    fun initStrengthAdapter() {
        strengthAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        strength.adapter = strengthAdapter
        strength.prompt = "strength"
        lifecycleScope.launchWhenCreated {
            vm.strengths.collectLatest { strengths ->
                val items = strengths.map { strength -> strength.name }
                strengthAdapter.clear()
                strengthAdapter.addAll(items)
                strength.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        strength.prompt = "strength"
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        val selected = strengths[position]
                        vm.selectStrength(selected)
                    }

                }
            }
        }
    }

    fun initDifficultyAdapter() {
        difficultyAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        difficulty.adapter = difficultyAdapter
        difficulty.prompt = "difficulty"
        lifecycleScope.launchWhenCreated {
            vm.difficulties.collectLatest { difficulties ->
                difficultyAdapter.clear()
                difficultyAdapter.addAll(difficulties.map { difficulty -> difficulty.name })
                difficulty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        difficulty.prompt = "strength"
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        val selected = difficulties[position]
                        vm.selectDifficulty(selected)
                    }
                }
            }
        }
    }

    fun initComponentsAdapter() {
        components.adapter = componentsAdapter
        lifecycleScope.launchWhenCreated {
            vm.components.observe(viewLifecycleOwner, Observer {
                componentsAdapter.submitList(it)
                componentsAdapter.notifyDataSetChanged()
            })
        }
    }

    fun initStepsAdapter() {
        steps.adapter = stepsAdapter
        lifecycleScope.launchWhenCreated {
            vm.steps.observe(viewLifecycleOwner, Observer {
                stepsAdapter.submitList(it)
                stepsAdapter.notifyDataSetChanged()
            })
        }
    }

    fun createComponent() {
        val dialog = CreateComponentDialog.newInstance()
        dialog.show(parentFragmentManager, "COMPONENT")
    }
    fun createStep() {
        val dialog = CreateStepDialog.newInstance()
        val args = Bundle()
        args.putInt(CreateStepDialog.KEY_STEP_NUMBER, vm.getStepCount())
        dialog.arguments = args
        dialog.show(parentFragmentManager, "STEP")
    }

    @AfterPermissionGranted(CAMERA_AND_STORAGE)
    private fun pickImage() {
        val perms = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (EasyPermissions.hasPermissions(requireContext(), *perms)) {
            Matisse.from(this@CreateFragment)
                .choose(MimeType.ofImage())
                .countable(false)
                .maxSelectable(1)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(GlideEngine())
                .forResult(PICK_IMAGE_REQUEST_CODE)
        } else {
            EasyPermissions.requestPermissions(
                this@CreateFragment, "get me permissions",
                CAMERA_AND_STORAGE,
                *perms
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults, this
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val result = Matisse.obtainPathResult(data)
            Log.d("PICK_IMAGE", "result: $result")
            vm.addImage(result.map { src -> File(src) }.firstOrNull())
        }
    }
}