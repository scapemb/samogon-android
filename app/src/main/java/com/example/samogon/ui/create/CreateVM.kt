package com.example.samogon.ui.create

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.example.samogon.data.local.model.Difficulty
import com.example.samogon.data.local.model.Strength
import com.example.samogon.data.repo.SamogonRepo
import com.example.samogon.utils.SingleLiveEvent
import com.example.samogon.utils.enums.CreateCocktailErrorType
import java.io.File

class CreateVM @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {
    val error = repo.errorsAtCreate
    val name = repo.nameForCreate
    val time = repo.preparationTimeForCreate
    val description = repo.descriptionForCreate
    val image = repo.imageForCreate
    val components = repo.componentsForCreate
    val steps = repo.stepsForCreate
    val volume = repo.volumeForCreate
    val stepNumber = repo.stepNumberForCreate
    val difficulties = repo.loadDifficulties()
    val strengths = repo.loadStrengths()

    val nameError: MutableLiveData<String> = MutableLiveData()
    val descriptionError: MutableLiveData<String> = MutableLiveData()
    val timeError: MutableLiveData<String> = MutableLiveData()
    val componentsError: MutableLiveData<String> = MutableLiveData()
    val stepsError: MutableLiveData<String> = MutableLiveData()

    val addPhotoEvent: SingleLiveEvent<Unit> = SingleLiveEvent()

    fun getStepCount() = (stepNumber.value ?: 0) + 1

    fun selectStrength(strength: Strength?) {
        repo.selectStrength(strength)
    }
    fun selectDifficulty(difficulty: Difficulty?) {
        repo.selectDifficulty(difficulty)
    }
    fun addImage(image: File?) {
        repo.addImageForCreate(image)
    }

    fun observeErrors(): LiveData<CreateCocktailErrorType> {
        val errors = MediatorLiveData<CreateCocktailErrorType>()
        errors.addSource(this.error) {
            errors.value = it

            nameError.value = ""
            descriptionError.value = ""
            componentsError.value = ""
            stepsError.value = ""

            when(it) {
                CreateCocktailErrorType.EMPTY_NAME -> {
                    nameError.value = "Empty name"
                }
                CreateCocktailErrorType.EMPTY_DESCRIPTION -> {
                    descriptionError.value = "Empty description"
                }
                CreateCocktailErrorType.EMPTY_TIME -> {
                    timeError.value = "Empty time"
                }
                CreateCocktailErrorType.EMPTY_COMPONENTS -> {
                    componentsError.value = "Empty components"
                }
                CreateCocktailErrorType.EMPTY_STEPS -> {
                    stepsError.value = "Empty steps"
                }
                CreateCocktailErrorType.TOO_LONG_NAME -> {
                    nameError.value = "Too long name"
                }
                CreateCocktailErrorType.TOO_LONG_DESCRIPTION -> {
                    descriptionError.value = "Too long description"
                }
            }
        }
        return errors
    }

    fun addPhoto() {
        addPhotoEvent.call()
    }
}