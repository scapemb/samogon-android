package com.example.samogon.ui.dialogs.createComponent

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.example.samogon.R
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.databinding.DialogCreateComponentBinding
import com.example.samogon.databinding.DialogSelectIngredientBinding
import com.example.samogon.ui.adapter.IngredientsAdapter
import com.example.samogon.ui.dialogs.selectIngredient.SelectIngredientDialog
import com.example.samogon.ui.dialogs.selectIngredient.SelectIngredientVM
import com.example.samogon.ui.viewholder.IngredientClickListener
import com.example.samogon.utils.listener.OnClickListener
import com.example.samogon.utils.listener.OnItemSelected
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class CreateComponentDialog : BottomSheetDialogFragment(), OnClickListener {
    private val vm by viewModels<CreateComponentVM>()
    lateinit var binding: DialogCreateComponentBinding

    var onIngredientSelectedListener: OnItemSelected<Ingredient>? = null

    companion object {
        fun newInstance() = CreateComponentDialog()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_create_component, container, false)

        binding.apply {
            lifecycleOwner = this@CreateComponentDialog
            vm = this@CreateComponentDialog.vm
            clickListener = this@CreateComponentDialog
        }
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        vm.apply {
            addEvent.observe(viewLifecycleOwner, Observer {
                lifecycleScope.cancel()
                dismissAllowingStateLoss()
            })
        }
    }

    override fun onClick() {
        val dialog = SelectIngredientDialog.newInstance()
        dialog.onIngredientSelectedListener = object: OnItemSelected<Ingredient> {
            override fun onSelected(item: Ingredient) {
                vm.selectIngredient(item)
            }
        }
        dialog.show(childFragmentManager, "INGREDIENT")
    }
}