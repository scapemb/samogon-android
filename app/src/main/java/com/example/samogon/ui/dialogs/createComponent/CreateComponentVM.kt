package com.example.samogon.ui.dialogs.createComponent

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.data.repo.SamogonRepo
import com.example.samogon.utils.SingleLiveEvent

class CreateComponentVM  @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {
    val addEvent: SingleLiveEvent<Unit> = SingleLiveEvent()

    val ingredient: MutableLiveData<Ingredient> = MutableLiveData(Ingredient.generatePreview())
    val isLiquid: MutableLiveData<Boolean> = MutableLiveData(false)
    val volume: MutableLiveData<String> = MutableLiveData("")

    fun observeIngredient(): LiveData<Ingredient> {
        val ingredient = MediatorLiveData<Ingredient>()
        ingredient.addSource(this.ingredient) {
            ingredient.value = it
            isLiquid.value = it.isLiquid()
        }
        return ingredient
    }

    fun selectIngredient(ingredient: Ingredient) {
        this.ingredient.value = ingredient
    }

    fun addComponent() {
        val ingredient = this.ingredient.value
        if(ingredient?.ingredientId?.isNotEmpty() == false) {
            return
        }
        if(this.volume.value.isNullOrEmpty() && ingredient?.isLiquid() == true) {
            return
        }
        val volume = when(this.volume.value.isNullOrEmpty()) {
            true -> "0"
            false -> this.volume.value!!
        }
        if(ingredient != null) {
            val component = CreateComponentRemote(
                ingredientId = ingredient.ingredientId,
                volume = Integer.parseInt(volume),
                ingredient = ingredient
            )
            repo.addComponentForCreate(component)
        }
        addEvent.call()
    }
}