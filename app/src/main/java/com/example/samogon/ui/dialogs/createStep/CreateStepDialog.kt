package com.example.samogon.ui.dialogs.createStep

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.samogon.R
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.databinding.DialogCreateComponentBinding
import com.example.samogon.databinding.DialogCreateStepBinding
import com.example.samogon.databinding.DialogSelectIngredientBinding
import com.example.samogon.ui.adapter.IngredientsAdapter
import com.example.samogon.ui.dialogs.selectIngredient.SelectIngredientDialog
import com.example.samogon.ui.dialogs.selectIngredient.SelectIngredientVM
import com.example.samogon.ui.viewholder.IngredientClickListener
import com.example.samogon.utils.listener.OnClickListener
import com.example.samogon.utils.listener.OnItemSelected
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class CreateStepDialog : BottomSheetDialogFragment(), OnClickListener {
    private val vm by viewModels<CreateStepVM>()
    lateinit var binding: DialogCreateStepBinding

    companion object {
        fun newInstance() = CreateStepDialog()
        val KEY_STEP_NUMBER = "KEY_STEP_NUMBER"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var stepNumber = with(arguments){
            this?.let {
                it.getInt(KEY_STEP_NUMBER)
            }
        }
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_create_step, container, false)

        binding.apply {
            lifecycleOwner = this@CreateStepDialog
            vm = this@CreateStepDialog.vm.apply {
                selectNumber(stepNumber)
            }
            clickListener = this@CreateStepDialog
        }
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        vm.apply {
            addEvent.observe(viewLifecycleOwner, Observer {
                lifecycleScope.cancel()
                dismissAllowingStateLoss()
            })
        }
    }

    override fun onClick() {
        vm.addStep()
    }
}