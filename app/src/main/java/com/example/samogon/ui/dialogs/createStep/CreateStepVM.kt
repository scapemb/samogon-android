package com.example.samogon.ui.dialogs.createStep

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.data.remote.model.CreateStepRemote
import com.example.samogon.data.repo.SamogonRepo
import com.example.samogon.utils.SingleLiveEvent

class CreateStepVM  @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {
    val addEvent: SingleLiveEvent<Unit> = SingleLiveEvent()

    val description: MutableLiveData<String> = MutableLiveData()
    val number: MutableLiveData<Int> = MutableLiveData()

    fun selectNumber(number: Int?) {
        this.number.value = number
    }

    fun addStep() {
        val description = this.description.value
        val number = this.number.value ?: 0
        if(description != null) {
            val step = CreateStepRemote(
                description, number
            )
            repo.addStepForCreate(step)
        }
        addEvent.call()
    }
}