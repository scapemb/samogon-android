package com.example.samogon.ui.dialogs.selectIngredient

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.samogon.R
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.databinding.DialogSelectIngredientBinding
import com.example.samogon.ui.adapter.IngredientsAdapter
import com.example.samogon.ui.viewholder.IngredientClickListener
import com.example.samogon.utils.listener.OnItemSelected
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_select_ingredient.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class SelectIngredientDialog : AppCompatDialogFragment(), IngredientClickListener {
    private val vm by viewModels<SelectIngredientVM>()
    lateinit var binding: DialogSelectIngredientBinding

    val job = Job()
    val uiScope = CoroutineScope(Dispatchers.Main + job)

    private val ingredientsAdapter = IngredientsAdapter(this)

    var onIngredientSelectedListener: OnItemSelected<Ingredient>? = null

    companion object {
        fun newInstance() = SelectIngredientDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_select_ingredient, null, false)

        binding.apply {
            lifecycleOwner = this@SelectIngredientDialog
            vm = this@SelectIngredientDialog.vm
        }

        return AlertDialog.Builder(requireContext(), R.style.commonDialogTheme)
            .setTitle("Select Ingredient")
            .setView(binding.root)
            .create()
    }

    override fun onStart() {
        super.onStart()
        initAdapter()
    }

    private fun initAdapter() {
        binding.list.adapter = ingredientsAdapter
        uiScope.launch {
            vm.ingredients.collectLatest {
                ingredientsAdapter.submitData(it)
            }
        }
    }

    override fun onClick(ingredient: Ingredient) {
        onIngredientSelectedListener?.onSelected(ingredient)
        lifecycleScope.cancel()
        uiScope.cancel()
        job.cancel()
        dismissAllowingStateLoss()
    }
}