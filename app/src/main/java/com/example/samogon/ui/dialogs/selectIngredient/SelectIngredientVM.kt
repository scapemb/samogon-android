package com.example.samogon.ui.dialogs.selectIngredient

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.samogon.data.repo.SamogonRepo

class SelectIngredientVM @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {
    val ingredients = repo.ingredients
        .cachedIn(viewModelScope)
}