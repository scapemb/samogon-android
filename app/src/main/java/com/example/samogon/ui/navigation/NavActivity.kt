package com.example.samogon.ui.navigation

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import androidx.activity.viewModels
import androidx.annotation.NonNull
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import androidx.work.*
import com.example.samogon.R
import com.example.samogon.databinding.ActivityHomeBinding
import com.example.samogon.ui.compilations.CompilationListFragment
import com.example.samogon.ui.search.SearchFragment
import com.example.samogon.utils.BindableActivity
import com.example.samogon.utils.service.CreateCocktailService
import com.example.samogon.utils.worker.CreateCocktailWorker
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.Behavior.DragCallback
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*


@AndroidEntryPoint
class NavActivity : BindableActivity() {
    private val binding: ActivityHomeBinding by bind(R.layout.activity_home)
    private val vm by viewModels<NavVM>()
    lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var bottomBarConfiguration: AppBarConfiguration
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.apply {
            lifecycleOwner = this@NavActivity
            vm = this@NavActivity.vm
        }
        navController = findNavController(R.id.home_nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
        }

        initBottomNav(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            handleAppBar(when(destination.id) {
                R.id.searchFragment, R.id.compilationListFragment -> true
                else -> false
            })
            vm.changeMode(when(destination.id) {
                R.id.createFragment -> true
                else -> false
            })
            bottom_bar.performShow()
        }

        subscribeToVM()
    }

    private fun subscribeToVM(){
        with(binding){
            vm?.let {
                it.startCreatingEvent.observe(this@NavActivity, Observer {
                    navController.navigate(R.id.createFragment)
                })
                it.createEvent.observe(this@NavActivity, Observer {
                    startCocktailUpload()
                    navController.navigateUp()
                })
            }
        }
    }

    private fun startCocktailUpload() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val uploadWork = OneTimeWorkRequest.Builder(CreateCocktailWorker::class.java)
            .addTag("work")
            .setConstraints(constraints)
            .build()

        WorkManager.getInstance(this)
            .enqueue(uploadWork)
    }

    private fun startCocktailUploadService() {
        val intent = Intent(this, CreateCocktailService::class.java).apply {
            action = CreateCocktailService.ACTION_UPLOAD
        }
        ContextCompat.startForegroundService(this, intent)
    }

    private fun handleAppBar(isLocked: Boolean) {
        val params = (toolbar.layoutParams as AppBarLayout.LayoutParams)
        params.scrollFlags = 0
        if (!isLocked) params.scrollFlags =
            AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
        toolbar.layoutParams = params

        if(isLocked) {
            supportActionBar?.hide()
        } else {
            supportActionBar?.show()
        }
    }

    private fun initBottomNav(navController: NavController) {
        navigation.setupWithNavController(navController)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        NavigationUI.onNavDestinationSelected(item, findNavController(R.id.home_nav_host_fragment))
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.home_nav_host_fragment)
        return navController.navigateUp()
                || super.onSupportNavigateUp()
    }
}