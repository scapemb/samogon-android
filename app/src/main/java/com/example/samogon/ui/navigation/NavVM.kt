package com.example.samogon.ui.navigation

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.samogon.R
import com.example.samogon.data.repo.SamogonRepo
import com.example.samogon.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class NavVM @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {
    val createEvent: SingleLiveEvent<Unit> = SingleLiveEvent()
    val startCreatingEvent: SingleLiveEvent<Unit> = SingleLiveEvent()
    val isCreateMode: MutableLiveData<Boolean> = MutableLiveData()
    val fabIcon: MutableLiveData<Int> = MutableLiveData(R.drawable.ic_add_black_24dp)

    fun changeMode(isCreateMode: Boolean) {
        this.isCreateMode.value = isCreateMode
        fabIcon.value = when(isCreateMode) {
            true -> R.drawable.ic_local_bar_black_24dp
            false -> R.drawable.ic_add_black_24dp
        }
    }

    fun fabClick() {
        if(isCreateMode.value == true) {
            viewModelScope.launch {
                if(repo.isCorrect()) {
                    createEvent.call()
                }
            }
        } else {
            startCreatingEvent.call()
        }
    }
}