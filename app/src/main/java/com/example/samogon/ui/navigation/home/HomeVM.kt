package com.example.samogon.ui.navigation.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.example.samogon.data.repo.SamogonRepo

class HomeVM  @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {
}