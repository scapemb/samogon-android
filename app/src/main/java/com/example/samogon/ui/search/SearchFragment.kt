package com.example.samogon.ui.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.samogon.R
import com.example.samogon.databinding.FragmentSearchBinding
import com.example.samogon.ui.adapter.BaseAdapter
import com.example.samogon.ui.adapter.DifficultyAdapter
import com.example.samogon.ui.adapter.StrengthAdapter
import com.example.samogon.ui.adapter.TasteAdapter
import com.example.samogon.utils.customView.BackdropBehavior
import com.example.samogon.utils.enums.CocktailListType
import com.example.samogon.utils.enums.SearchMode
import com.example.samogon.utils.extension.FragmentExtensions.findBehavior
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@AndroidEntryPoint
class SearchFragment: Fragment(), BackdropBehavior.OnDropListener {
    private val vm: SearchVM by activityViewModels()
    private lateinit var backdropBehavior: BackdropBehavior
    private lateinit var baseAdapter: BaseAdapter
    private lateinit var tasteAdapter: TasteAdapter
    private lateinit var strengthAdapter: StrengthAdapter
    private lateinit var difficultyAdapter: DifficultyAdapter
    private var searchView: SearchView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentSearchBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_search, container, false
        )
        binding.apply {
            lifecycleOwner = this@SearchFragment
            vm = this@SearchFragment.vm
        }
        //setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backdropBehavior = content.findBehavior()
        with(backdropBehavior) {
            toolbarSearch?.let {
                attachBackLayout(R.id.back)
                attachToolbar(R.id.toolbarSearch)
                attachToolbar(it)
                addOnDropListener(this@SearchFragment)
                initSearch(it)
            }
        }
    }

    private fun initSearch(toolbar: Toolbar) {
        with(toolbar){
            inflateMenu(R.menu.menu_search)
            val searchItem = menu.findItem(R.id.action_search)
            with(searchItem) {
                val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as? SearchManager
                searchView = actionView as SearchView
                searchView?.let{
                    with(it) {
                        setSearchableInfo(searchManager?.getSearchableInfo(activity?.componentName))
                        setOnQueryTextListener(
                            object : SearchView.OnQueryTextListener {
                                lateinit var inputDelayJob: Job
                                override fun onQueryTextSubmit(query: String?): Boolean {
                                    vm.search(query)
                                    searchView?.clearFocus()
                                    return true
                                }

                                override fun onQueryTextChange(newText: String?): Boolean {
                                    if (::inputDelayJob.isInitialized) {
                                        inputDelayJob.cancel()
                                    }
                                    inputDelayJob = viewLifecycleOwner.lifecycleScope.launch {
                                        delay(500)
                                        vm.search(newText)
                                    }
                                    return true
                                }

                            })
                        setOnSearchClickListener {
                            vm.switchSearchMode(SearchMode.TEXT)
                        }
                        setOnCloseListener {
                            vm.clearSearch()
                            false
                        }
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        subscribeToVm()
        initBaseAdapter()
        initTasteAdapter()
        initStrengthAdapter()
        initDifficultyAdapter()
    }

    private fun subscribeToVm() {
        vm.apply {
            searchMode.observe(viewLifecycleOwner, Observer { it ->
                if(it == SearchMode.TEXT) {
                    backdropBehavior.close(true)
                    vm.clearFilters()
                } else {
                    searchView?.let {
                        if (!it.isIconified) {
                            it.onActionViewCollapsed()
                        }
                    }
                }
            })
            openCocktailEvent.observe(viewLifecycleOwner, Observer {
                navigate(SearchFragmentDirections.actionSearchFragmentToCocktailFragment(it.cocktailId))
            })
            openIngredientEvent.observe(viewLifecycleOwner, Observer {
                navigate(SearchFragmentDirections.actionSearchFragmentToCocktailListFragment(it.ingredientId, CocktailListType.COCKTAILS_FOR_INGREDIENT))
            })
        }
    }

    private fun navigate(action: NavDirections) {
        findNavController().navigate(action)
    }

    override fun onStop() {
        super.onStop()
        with(backdropBehavior) {
            removeDropListener(this@SearchFragment)
        }
    }

    private fun initBaseAdapter() {
        baseAdapter = BaseAdapter(vm)
        with(bases) {
            adapter = baseAdapter
            layoutManager = FlexboxLayoutManager(context).apply {
                flexDirection = FlexDirection.ROW
                flexWrap = FlexWrap.WRAP
                justifyContent = JustifyContent.FLEX_START
            }
        }
        vm.observeBases().observe(viewLifecycleOwner, Observer {
            baseAdapter.submitList(it)
        })
    }

    private fun initTasteAdapter() {
        tasteAdapter = TasteAdapter(vm)
        with(tastes) {
            adapter = tasteAdapter
            layoutManager = FlexboxLayoutManager(context).apply {
                flexDirection = FlexDirection.ROW
                flexWrap = FlexWrap.WRAP
                justifyContent = JustifyContent.FLEX_START
            }
        }
        vm.observeTastes().observe(viewLifecycleOwner, Observer {
            tasteAdapter.submitList(it)
        })
    }

    private fun initStrengthAdapter() {
        strengthAdapter = StrengthAdapter(vm)
        strengths.adapter = strengthAdapter
        vm.observeStrengths().observe(viewLifecycleOwner, Observer {
            strengthAdapter.submitList(it)
        })
    }

    private fun initDifficultyAdapter() {
        difficultyAdapter = DifficultyAdapter(vm)
        difficulties.adapter = difficultyAdapter
        vm.observeDifficulties().observe(viewLifecycleOwner, Observer {
            difficultyAdapter.submitList(it)
        })
    }



    override fun onDrop(dropState: BackdropBehavior.DropState, fromUser: Boolean) {
        when(dropState) {
            BackdropBehavior.DropState.OPEN -> {
                vm.switchSearchMode(SearchMode.FILTER)
            }
            BackdropBehavior.DropState.CLOSE -> {
                if(!fromUser) {
                    vm.switchSearchMode(SearchMode.TEXT)
                } else {
                    vm.refreshFilteredResults()
                }
            }
        }
        activity?.invalidateOptionsMenu()
    }
}