package com.example.samogon.ui.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.databinding.FragmentListSearchResultsBinding
import com.example.samogon.ui.adapter.CocktailsAdapter
import com.example.samogon.ui.adapter.IngredientsAdapter
import com.example.samogon.ui.viewholder.IngredientClickListener
import com.example.samogon.utils.listener.CocktailClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_list_cocktails.*
import kotlinx.android.synthetic.main.fragment_list_search_results.*
import kotlinx.android.synthetic.main.fragment_list_search_results.list
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class SearchResultListFragment: Fragment(), CocktailClickListener, IngredientClickListener {
    private val vm: SearchVM by activityViewModels()

    private val cocktailsAdapter = CocktailsAdapter(this)
    private val filteredCocktailsAdapter = CocktailsAdapter(this)
    private val ingredientsAdapter = IngredientsAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentListSearchResultsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_list_search_results, container, false
        )
        binding.apply {
            lifecycleOwner = this@SearchResultListFragment
            vm = this@SearchResultListFragment.vm
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        subscribeToVm()
    }

    private fun subscribeToVm() {
        vm.apply {
            observeSearchClause().observe(viewLifecycleOwner, Observer {
                cocktailsAdapter.refresh()
                ingredientsAdapter.refresh()
            })
            refreshFilteredResultsEvent.observe(viewLifecycleOwner, Observer {
                this@SearchResultListFragment.filteredCocktailsAdapter.refresh()
            })
        }
    }

    private fun initAdapter() {
        list.adapter = ConcatAdapter(ingredientsAdapter, cocktailsAdapter, filteredCocktailsAdapter)
        list.emptyView = empty
        lifecycleScope.launchWhenCreated {
            vm.filteredCocktails.collectLatest {
                filteredCocktailsAdapter.submitData(it)
            }
        }
        lifecycleScope.launchWhenCreated {
            vm.cocktails.collectLatest {
                cocktailsAdapter.submitData(it)
            }
        }
        lifecycleScope.launchWhenCreated {
            vm.ingredients.collectLatest {
                ingredientsAdapter.submitData(it)
            }
        }
    }

    override fun onClick(cocktail: Cocktail) {
        vm.openCocktail(cocktail)
    }

    override fun onClick(ingredient: Ingredient) {
        vm.openIngredient(ingredient)
    }
}