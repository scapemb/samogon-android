package com.example.samogon.ui.search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.example.samogon.data.local.model.*
import com.example.samogon.data.repo.SamogonRepo
import com.example.samogon.utils.SingleLiveEvent
import com.example.samogon.utils.enums.SearchMode
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class SearchVM @ViewModelInject constructor(
    val repo: SamogonRepo
) : ViewModel() {

    //TODO: retrieve bases from backend

    val refreshFilteredResultsEvent: SingleLiveEvent<Unit> = SingleLiveEvent()

    val openCocktailEvent: SingleLiveEvent<Cocktail> = SingleLiveEvent()
    val openIngredientEvent: SingleLiveEvent<Ingredient> = SingleLiveEvent()

    val searchClause: MutableLiveData<String> = MutableLiveData()
    val searchMode: MutableLiveData<SearchMode> = MutableLiveData(repo.searchMode)
    val bases: MutableLiveData<MutableList<Base>> = MutableLiveData(mutableListOf())
    val tastes: MutableLiveData<MutableList<Taste>> = MutableLiveData(mutableListOf())
    val strengths: MutableLiveData<MutableList<Strength>> = MutableLiveData(mutableListOf())
    val difficulties: MutableLiveData<MutableList<Difficulty>> = MutableLiveData(mutableListOf())

    var filteredCocktails = repo.filteredCocktails
        .cachedIn(viewModelScope)
    val cocktails= repo.searchCocktails
        .cachedIn(viewModelScope)
    val ingredients = repo.searchIngredients
        .cachedIn(viewModelScope)

    init {
        loadBases()
        loadTastes()
        loadStrengths()
        loadDifficulties()
    }

    fun openCocktail(cocktail: Cocktail) {
        openCocktailEvent.value = cocktail
    }
    fun openIngredient(ingredient: Ingredient) {
        openIngredientEvent.value = ingredient
    }

    fun switchSearchMode(mode: SearchMode) {
        repo.searchMode = mode
        searchMode.value = mode
    }
    fun clearSearch() {
        search(null)
    }
    fun clearFilters() {
        clearBase()
        clearTastes()
        clearStrength()
        clearDifficulty()
        refreshFilteredResultsEvent.call()
    }

    fun refreshFilteredResults() {
        clearSearch()
        refreshFilteredResultsEvent.call()
    }

    fun observeSearchClause(): LiveData<String> {
        val clause = MediatorLiveData<String>()
        clause.addSource(this.searchClause) {
            clause.value = it
        }
        return clause
    }
    fun observeBases(): LiveData<MutableList<Base>> {
        val bases = MediatorLiveData<MutableList<Base>>()
        bases.addSource(this.bases) {
            bases.value = it
        }
        return bases
    }
    fun observeTastes(): LiveData<MutableList<Taste>> {
        val tastes = MediatorLiveData<MutableList<Taste>>()
        tastes.addSource(this.tastes) {
            tastes.value = it
        }
        return tastes
    }
    fun observeStrengths(): LiveData<MutableList<Strength>> {
        val strengths = MediatorLiveData<MutableList<Strength>>()
        strengths.addSource(this.strengths) {
            strengths.value = it
        }
        return strengths
    }
    fun observeDifficulties(): LiveData<MutableList<Difficulty>> {
        val difficulties = MediatorLiveData<MutableList<Difficulty>>()
        difficulties.addSource(this.difficulties) {
            difficulties.value = it
        }
        return difficulties
    }

    fun loadBases() {
        viewModelScope.launch {
            repo.loadBases().collectLatest {
                bases.value = it.toMutableList()
            }
        }
    }

    fun loadTastes() {
        viewModelScope.launch {
            repo.loadTastes().collectLatest {
                tastes.value = it.toMutableList()
            }
        }
    }

    fun loadStrengths() {
        viewModelScope.launch {
            repo.loadStrengths().collectLatest {
                strengths.value = it.toMutableList()
            }
        }
    }

    fun loadDifficulties() {
        viewModelScope.launch {
            repo.loadDifficulties().collectLatest {
                difficulties.value = it.toMutableList()
            }
        }
    }

    fun search(clause: String?) {
        repo.searchClause = clause
        searchClause.value = clause
    }

    fun selectBase(id: String) {
        bases.value?.let {
            val base = it.find { it.baseId == id }
            base?.isSelected = base?.isSelected?.not() ?: false
            repo.bases = it.filter { it.isSelected }.map { it.baseId }
            bases.postValue(it)
        }
    }
    fun clearBase(){
        repo.bases = null
    }

    fun selectStrength(id: String) {
        strengths.value?.let {
            val strength = it.find { it.strengthId == id }
            strength?.isSelected = strength?.isSelected?.not() ?: false
            repo.strengths = it.filter { it.isSelected }.map { it.strengthId }
            strengths.postValue(it)
        }
    }
    fun clearStrength(){
        repo.strengths = null
    }

    fun selectDifficulty(id: String) {
        difficulties.value?.let {
            val difficulty = it.find { it.difficultyId == id }
            difficulty?.isSelected = difficulty?.isSelected?.not() ?: false
            repo.difficulties = it.filter { it.isSelected }.map { it.difficultyId }
            difficulties.postValue(it)
        }
    }
    fun clearDifficulty(){
        repo.difficulties = null
    }

    fun selectTaste(id: String) {
        tastes.value?.let {
            val taste = it.find { it.tasteId == id }
            taste?.isSelected = taste?.isSelected?.not() ?: false
            repo.tastes = it.filter { it.isSelected }.map { it.tasteId }
            tastes.postValue(it)
        }
    }
    fun clearTastes(){
        repo.tastes = null
    }

}