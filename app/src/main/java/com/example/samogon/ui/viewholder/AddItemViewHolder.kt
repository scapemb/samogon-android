package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.databinding.AddViewItemBinding
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.databinding.ComponentCreateViewItemBinding
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.utils.listener.CocktailClickListener
import com.example.samogon.utils.listener.OnClickListener

class AddItemViewHolder(val binding: AddViewItemBinding, val listener: OnClickListener): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup, listener: OnClickListener): AddItemViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = AddViewItemBinding.inflate(inflater, parent, false)
            return AddItemViewHolder(binding, listener)
        }
    }

    fun bind() {
        with(binding) {
            clickListener = listener
        }
    }
}