package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.ui.search.SearchVM

class BaseViewHolder(val binding: BaseViewItemBinding, val vm: SearchVM): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup,  vm : SearchVM): BaseViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = BaseViewItemBinding.inflate(inflater, parent, false)
            return BaseViewHolder(binding, vm)
        }
    }

    val base: MutableLiveData<Base> = MutableLiveData()

    fun bind(base: Base?) {
        with(binding) {
            this.base = base
            searchVm = vm
        }
    }
}
/*

interface IngredientClickListener {
    fun onClick(ingredient: Ingredient)
}*/
