package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.CocktailWithTastes
import com.example.samogon.databinding.CocktailViewItemBinding
import com.example.samogon.utils.listener.CocktailClickListener

class CocktailViewHolder(val binding: CocktailViewItemBinding, val listener: CocktailClickListener): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup, listener: CocktailClickListener): CocktailViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = CocktailViewItemBinding.inflate(inflater, parent, false)
            return CocktailViewHolder(binding, listener)
        }
    }

    val cocktail: MutableLiveData<Cocktail> = MutableLiveData()

    fun bind(cocktail: Cocktail?) {
        with(binding) {
            this.cocktail = cocktail
            clickListener = listener
        }
    }
}