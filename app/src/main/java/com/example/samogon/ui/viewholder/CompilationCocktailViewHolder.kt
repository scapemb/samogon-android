package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.databinding.CompilationCocktailViewItemBinding
import com.example.samogon.utils.listener.CocktailClickListener

class CompilationCocktailViewHolder(val binding: CompilationCocktailViewItemBinding,  val listener: CocktailClickListener): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup, listener: CocktailClickListener): CompilationCocktailViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = CompilationCocktailViewItemBinding.inflate(inflater, parent, false)
            return CompilationCocktailViewHolder(binding, listener)
        }
    }

    val cocktail: MutableLiveData<Cocktail> = MutableLiveData()

    fun bind(cocktail: Cocktail?) {
        with(binding) {
            this.cocktail = cocktail
            this.clickListener = listener
        }
    }
}
/*

interface IngredientClickListener {
    fun onClick(ingredient: Ingredient)
}*/
