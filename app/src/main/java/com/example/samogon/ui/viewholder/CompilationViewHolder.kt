package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.databinding.CompilationViewItemBinding
import com.example.samogon.ui.adapter.BaseAdapter
import com.example.samogon.ui.adapter.CompilationAdapter
import com.example.samogon.ui.adapter.TasteAdapter
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.utils.listener.CocktailClickListener
import com.example.samogon.utils.listener.CompilationClickListener
import com.example.samogon.utils.listener.OnClickListener

class CompilationViewHolder(val binding: CompilationViewItemBinding, val seeAllListener: CompilationClickListener, val cocktailListener: CocktailClickListener): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup, seeAllListener: CompilationClickListener, listener: CocktailClickListener): CompilationViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = CompilationViewItemBinding.inflate(inflater, parent, false)
            return CompilationViewHolder(binding, seeAllListener, listener)
        }
    }

    val compilation: MutableLiveData<CompilationWithCocktails> = MutableLiveData()
    private lateinit var cocktailAdapter: CompilationAdapter

    fun bind(compilation: CompilationWithCocktails?) {
        cocktailAdapter = CompilationAdapter(cocktailListener)
        val snapHelper = LinearSnapHelper()
        with(binding) {
            this.clickListener = seeAllListener
            this.compilation = compilation
            with(this.list) {
                adapter = cocktailAdapter
                snapHelper.attachToRecyclerView(this)
            }
            compilation?.cocktails?.let {
                cocktailAdapter.submitList(it)
            }
        }
    }
}
/*

interface IngredientClickListener {
    fun onClick(ingredient: Ingredient)
}*/
