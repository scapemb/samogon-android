package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.databinding.ComponentCreateViewItemBinding
import com.example.samogon.ui.search.SearchVM

class ComponentForCreateViewHolder(val binding: ComponentCreateViewItemBinding): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup): ComponentForCreateViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ComponentCreateViewItemBinding.inflate(inflater, parent, false)
            return ComponentForCreateViewHolder(binding)
        }
    }

    fun bind(component: CreateComponentRemote) {
        with(binding) {
            this.component = component
        }
    }
}