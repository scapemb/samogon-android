package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.samogon.R
import com.example.samogon.data.local.model.*
import com.example.samogon.databinding.CocktailViewItemBinding
import com.example.samogon.databinding.ComponentViewItemBinding
import com.example.samogon.ui.adapter.ComponentsAdapter

class ComponentViewHolder(val adapter: ComponentsAdapter, val binding: ComponentViewItemBinding, val listener: ComponentClickListener): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(adapter: ComponentsAdapter, parent: ViewGroup, listener: ComponentClickListener): ComponentViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ComponentViewItemBinding.inflate(inflater, parent, false)
            return ComponentViewHolder(adapter, binding, listener)
        }
    }

    val component: MutableLiveData<Component> = MutableLiveData()
    val ingredient: MutableLiveData<Ingredient> = MutableLiveData()
    val volume: MutableLiveData<String> = MutableLiveData()

    fun bind(component: Component?) {
        binding.vh = this
        binding.component = component
        binding.clickListener = listener
        component?.ingredient?.let {
            binding.ingredient = it
        }
        with(adapter) {
            component?.let {
                if(cocktailParts != 0) {
                    volume.value = "${((cocktailVolume / cocktailParts) * it.value)} ml"
                }
            }
        }
    }
}

interface ComponentClickListener {
    fun onClick(component: Component)
}