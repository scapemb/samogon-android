package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.databinding.DifficultyViewItemBinding
import com.example.samogon.databinding.StrengthViewItemBinding
import com.example.samogon.ui.search.SearchVM

class DifficultyViewHolder(val binding: DifficultyViewItemBinding, val vm: SearchVM): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup,  vm : SearchVM): DifficultyViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = DifficultyViewItemBinding.inflate(inflater, parent, false)
            return DifficultyViewHolder(binding, vm)
        }
    }

    val difficulty: MutableLiveData<Difficulty> = MutableLiveData()

    fun bind(difficulty: Difficulty?) {
        with(binding) {
            this.difficulty = difficulty
            searchVm = vm
        }
    }
}
/*

interface IngredientClickListener {
    fun onClick(ingredient: Ingredient)
}*/
