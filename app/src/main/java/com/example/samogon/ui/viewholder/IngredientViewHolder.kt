package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.CocktailWithTastes
import com.example.samogon.data.local.model.Ingredient
import com.example.samogon.databinding.CocktailViewItemBinding
import com.example.samogon.databinding.IngredientViewItemBinding

class IngredientViewHolder(val binding: IngredientViewItemBinding, val listener: IngredientClickListener): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup, listener: IngredientClickListener): IngredientViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = IngredientViewItemBinding.inflate(inflater, parent, false)
            return IngredientViewHolder(binding, listener)
        }
    }

    val cocktail: MutableLiveData<Cocktail> = MutableLiveData()

    fun bind(ingredient: Ingredient?) {
        with(binding) {
            this.ingredient = ingredient
            clickListener = listener
        }
    }
}

interface IngredientClickListener {
    fun onClick(ingredient: Ingredient)
}