package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.data.remote.model.CreateComponentRemote
import com.example.samogon.data.remote.model.CreateStepRemote
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.databinding.ComponentCreateViewItemBinding
import com.example.samogon.databinding.StepCreateViewItemBinding
import com.example.samogon.ui.search.SearchVM

class StepForCreateViewHolder(val binding: StepCreateViewItemBinding): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup): StepForCreateViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = StepCreateViewItemBinding.inflate(inflater, parent, false)
            return StepForCreateViewHolder(binding)
        }
    }

    fun bind(step: CreateStepRemote) {
        with(binding) {
            this.step = step
        }
    }
}