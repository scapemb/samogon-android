package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.CocktailWithTastes
import com.example.samogon.data.local.model.Step
import com.example.samogon.databinding.CocktailViewItemBinding
import com.example.samogon.databinding.StepViewItemBinding

class StepViewHolder(val binding: StepViewItemBinding): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup): StepViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = StepViewItemBinding.inflate(inflater, parent, false)
            return StepViewHolder(binding)
        }
    }

    val step: MutableLiveData<Step> = MutableLiveData()

    fun bind(step: Step?) {
        binding.step = step
    }
}