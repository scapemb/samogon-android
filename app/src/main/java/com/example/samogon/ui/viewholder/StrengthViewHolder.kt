package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.databinding.StrengthViewItemBinding
import com.example.samogon.ui.search.SearchVM

class StrengthViewHolder(val binding: StrengthViewItemBinding, val vm: SearchVM): RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup,  vm : SearchVM): StrengthViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = StrengthViewItemBinding.inflate(inflater, parent, false)
            return StrengthViewHolder(binding, vm)
        }
    }

    val strength: MutableLiveData<Strength> = MutableLiveData()

    fun bind(strength: Strength?) {
        with(binding) {
            this.strength = strength
            searchVm = vm
        }
    }
}
/*

interface IngredientClickListener {
    fun onClick(ingredient: Ingredient)
}*/
