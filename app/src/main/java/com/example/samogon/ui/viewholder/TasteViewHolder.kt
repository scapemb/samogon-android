package com.example.samogon.ui.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.samogon.data.local.model.*
import com.example.samogon.databinding.BaseViewItemBinding
import com.example.samogon.databinding.TasteViewItemBinding
import com.example.samogon.ui.search.SearchVM
import com.example.samogon.utils.listener.TasteClickListener

class TasteViewHolder(val binding: TasteViewItemBinding, val vm: SearchVM?): RecyclerView.ViewHolder(binding.root), TasteClickListener {
    companion object {
        fun create(parent: ViewGroup,  vm : SearchVM?): TasteViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = TasteViewItemBinding.inflate(inflater, parent, false)
            return TasteViewHolder(binding, vm)
        }
    }

    val taste: MutableLiveData<Taste> = MutableLiveData()
    val isSelectable: MutableLiveData<Boolean> = MutableLiveData(false)

    fun bind(taste: Taste?) {
        with(binding) {
            this.taste = taste
            clickListener = this@TasteViewHolder
            isSelectable = vm != null
        }
    }

    override fun onClick(taste: Taste) {
        vm?.selectTaste(taste.tasteId)
    }
}
/*

interface IngredientClickListener {
    fun onClick(ingredient: Ingredient)
}*/
