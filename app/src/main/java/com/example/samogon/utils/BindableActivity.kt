package com.example.samogon.utils

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BindableActivity : AppCompatActivity() {
    protected inline fun <reified T : ViewDataBinding> bind(@LayoutRes resId: Int) : Lazy<T> = lazy { DataBindingUtil.setContentView<T>(this, resId) }
}