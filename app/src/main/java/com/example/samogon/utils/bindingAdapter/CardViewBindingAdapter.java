package com.example.samogon.utils.bindingAdapter;

import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.palette.graphics.Palette;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.samogon.R;
import com.google.android.material.card.MaterialCardView;

public class CardViewBindingAdapter {
    @BindingAdapter("bind:cocktailImageUrl")
    public static void bindCocktailImageUrl(MaterialCardView cardView, String url) {
        Glide.with(cardView.getContext())
                .asBitmap()
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.default_glassware)
                .error(R.drawable.default_glassware)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        if (resource != null) {
                            Palette.from(resource).generate(palette -> {
                                if(palette != null) {
                                    cardView.setCardBackgroundColor(palette.getVibrantColor(ContextCompat.getColor(cardView.getContext(), R.color.design_default_color_background)));
                                }
                            });
                        }
                        return false;
                    }
                }).submit();
    }
}
