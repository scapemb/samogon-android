package com.example.samogon.utils.bindingAdapter;

import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;

import androidx.databinding.BindingAdapter;

import com.google.android.material.chip.Chip;

public class ChipBindingAdapter {

    @BindingAdapter("bind:text")
    public static void bindText(Chip chip, String text) {
        chip.setText(text);
    }

    @BindingAdapter("bind:isSelected")
    public static void bindSelection(Chip chip, boolean isSelected) {
        chip.setSelected(isSelected);
        chip.setChecked(isSelected);
    }

    @BindingAdapter("bind:isSelectable")
    public static void bindSelectable(Chip chip, boolean isSelectable) {
        chip.setCheckable(isSelectable);
    }

    @BindingAdapter("bind:circleColor")
    public static void bindColorCircle(Chip chip, int color) {
        ShapeDrawable circle = new ShapeDrawable(new OvalShape());
        /*circle.setIntrinsicHeight (height);
        circle.setIntrinsicWidth (width);*/
        circle.getPaint ().setColor (color);
        chip.setChipIcon(circle);
    }
}