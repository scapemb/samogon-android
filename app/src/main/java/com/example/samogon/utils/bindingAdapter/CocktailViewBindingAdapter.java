package com.example.samogon.utils.bindingAdapter;

import android.graphics.Bitmap;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.palette.graphics.Palette;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.samogon.R;
import com.example.samogon.data.local.model.Cocktail;
import com.example.samogon.utils.customView.CocktailPreview;
import com.example.samogon.utils.customView.CocktailView;

public class CocktailViewBindingAdapter {
    @BindingAdapter("bind:cocktailImageUrl")
    public static void bindCocktailImageUrl(CocktailView view, String url) {
        Glide.with(view.getContext())
                .asBitmap()
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.default_glassware)
                .error(R.drawable.default_glassware)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        if (resource != null) {
                            Palette.from(resource).generate(palette -> {
                                if(palette != null) {
                                    view.attachPalette(palette);
                                }
                            });
                        }
                        return false;
                    }
                }).into(view.getImageView());
    }

    @BindingAdapter("bind:text")
    public static void setText(CocktailView view, String text) {
        view.getName().setText(text);
    }

    @BindingAdapter("bind:cocktail")
    public static void setCocktail(CocktailView view, Cocktail cocktail) {
        view.attachCocktail(cocktail);
    }
}
