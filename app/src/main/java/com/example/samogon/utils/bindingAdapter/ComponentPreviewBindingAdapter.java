package com.example.samogon.utils.bindingAdapter;

import android.graphics.Bitmap;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.palette.graphics.Palette;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.samogon.R;
import com.example.samogon.data.local.model.Cocktail;
import com.example.samogon.data.local.model.Component;
import com.example.samogon.utils.customView.CocktailPreview;
import com.example.samogon.utils.customView.ComponentPreview;

public class ComponentPreviewBindingAdapter {
    @BindingAdapter("bind:componentImageUrl")
    public static void bindComponentImageUrl(ComponentPreview componentPreview, String url) {
        Glide.with(componentPreview.getContext())
                .asBitmap()
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.default_glassware)
                .error(R.drawable.default_glassware)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        if (resource != null) {
                            Palette.from(resource).generate(palette -> {
                                if(palette != null) {
                                    componentPreview.attachPalette(palette);
                                }
                            });
                        }
                        return false;
                    }
                }).into(componentPreview.getImageView());
    }

    @BindingAdapter("bind:text")
    public static void setText(ComponentPreview componentPreview, String text) {
        componentPreview.getName().setText(text);
    }

    @BindingAdapter("bind:volume")
    public static void setVolume(ComponentPreview componentPreview, String volume) {
        componentPreview.getVolume().setText(volume);
    }

    @BindingAdapter("bind:component")
    public static void setComponent(ComponentPreview componentPreview, Component component) {
        componentPreview.attachComponent(component);
    }
}
