package com.example.samogon.utils.bindingAdapter;

import androidx.databinding.BindingAdapter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class FabBindingAdapter {
    @BindingAdapter("bind:fabSrc")
    public static void setFabSrc(final FloatingActionButton fab, final int res) {
        fab.setImageResource(res);
    }
}
