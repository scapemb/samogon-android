package com.example.samogon.utils.bindingAdapter;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.samogon.R;

import java.io.File;

public class ImageViewBindingAdapter {
    @BindingAdapter("bind:imageRes")
    public static void bindImageRes(ImageView imageView, int resource) {
        if(resource != 0) {
            Glide.with(imageView.getContext())
                    .load(resource)
                    //.placeholder(R.drawable.placeholder)
                    .into(imageView);
        }
    }

    @BindingAdapter("bind:cocktailImageRes")
    public static void bindCocktailImageRes(ImageView imageView, int resource) {
        if(resource != 0) {
            Glide.with(imageView.getContext())
                    .load(resource)
                    .placeholder(R.drawable.default_glassware)
                    .error(R.drawable.default_glassware)
                    .circleCrop()
                    .into(imageView);

        }
    }

    @BindingAdapter("bind:cocktailImageUrlCircle")
    public static void bindCocktailImageUrlCircle(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.default_glassware)
                .error(R.drawable.default_glassware)
                .circleCrop()
                .into(imageView);
    }

    @BindingAdapter("bind:cocktailImageUrl")
    public static void bindCocktailImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .transform(new RoundedCorners(8))
                .placeholder(R.drawable.default_glassware)
                .error(R.drawable.default_glassware)
                .into(imageView);
    }

    @BindingAdapter("bind:imageUrl")
    public static void bindImageUrl(ImageView imageView, String url) {
        if(url != null && !url.isEmpty()) {
            Glide.with(imageView.getContext())
                    .load(url)
                    .into(imageView);
        }
    }

    @BindingAdapter("bind:imageFile")
    public static void bindImageFile(ImageView imageView, File file) {
        if(file != null) {
            Log.d("IMGGY", "binding file: " + file);
            Glide.with(imageView.getContext())
                    .load(file)
                    .into(imageView);
        }
    }

    @BindingAdapter("bind:imageSource")
    public static void bindImageSource(ImageView imageView, Object source) {
        if(source != null) {
            Log.d("IMGGY", "binding source: " + source);
            Glide.with(imageView.getContext())
                    .load(source)
                    .placeholder(R.drawable.default_glassware)
                    .error(R.drawable.default_glassware)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            Log.d("IMAGE", "onLoadFailed");
                            if(e != null) {
                                e.printStackTrace();
                            }
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            Log.d("IMAGE", "onResourceReady");
                            return false;
                        }
                    })
                    .into(imageView);
        }
    }

    @BindingAdapter("bind:circleImageUrl")
    public static void bindCircleImageUrl(ImageView imageView, String imageUrl) {
        Glide.with(imageView.getContext())
                .load(imageUrl)
                .placeholder(R.mipmap.ic_launcher)
                .circleCrop()
                .into(imageView);
    }

    @BindingAdapter("bind:circleColor")
    public static void bindColorCircle(ImageView imageView, int color) {
        imageView.setBackgroundColor(color);
    }

    @BindingAdapter("bind:isVisible")
    public static void bindAlphaVisibility(ImageView imageView, float alpha) {
        if(alpha == 0.0){
            imageView.setVisibility(View.GONE);
        }else {
            imageView.setVisibility(View.VISIBLE);
        }
    }
}
