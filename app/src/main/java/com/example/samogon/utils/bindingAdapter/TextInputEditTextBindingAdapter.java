package com.example.samogon.utils.bindingAdapter;

import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;

import com.google.android.material.chip.Chip;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class TextInputEditTextBindingAdapter {

    @BindingAdapter("bind:text")
    public static void setText(TextInputEditText editTextView, double text) {
        editTextView.setText(String.valueOf(text));
    }
    @BindingAdapter("bind:text")
    public static void setText(TextInputEditText editTextView, Integer text) {
        if(editTextView.getText() != null && Integer.parseInt(editTextView.getText().toString()) != text) {
            editTextView.setText(String.valueOf(text));
        }
    }
   /* @InverseBindingAdapter(attribute = "bind:text")
    public static Integer getText(TextInputEditText editTextView) {
        if(editTextView.getText() != null) {
            try {
                return Integer.parseInt(editTextView.getText().toString());
            } catch (NumberFormatException e) {
                return 0;
            }
        }
        return 0;
    }*/
    @BindingAdapter("bind:text")
    public static void bindEditText(TextInputEditText editTextView, String text) {
        if(editTextView.getText() != null && !editTextView.getText().toString().equals(text)) {
            editTextView.setText(text);
        }
    }

    @BindingAdapter("bind:isVisible")
    public static void setVisibility(final TextInputLayout editTextView, final Boolean isVisible) {
        if(isVisible){
            editTextView.setVisibility(View.VISIBLE);
        }else {
            editTextView.setVisibility(View.GONE);
        }
    }


    @BindingAdapter("bind:error")
    public static void bindError(TextInputLayout layout, String text) {
        boolean isErrorVisible = text != null && !text.isEmpty();
        layout.setErrorEnabled(isErrorVisible);
        if(isErrorVisible){
            layout.setError(text);
        } else {
            layout.setError(null);
        }
    }
}