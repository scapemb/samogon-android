package com.example.samogon.utils.bindingAdapter;

import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.example.samogon.R;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;

public class TextViewBindingAdapter {
    @BindingAdapter("bind:text")
    public static void setText(TextView textView, int text) {
        textView.setText(String.valueOf(text));
    }

    @BindingAdapter("bind:text")
    public static void setText(TextView textView, long text) {
        textView.setText(String.valueOf(text));
    }

    @BindingAdapter("bind:isBold")
    public static void setBold(TextView view, boolean isBold) {
        if (isBold) {
            view.setTypeface(null, Typeface.BOLD);
        } else {
            view.setTypeface(null, Typeface.NORMAL);
        }
    }

    @BindingAdapter("bind:isVisible")
    public static void setVisibility(final TextView view, final Boolean isVisible) {
        if(isVisible){
            view.setVisibility(View.VISIBLE);
        }else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("bind:error")
    public static void bindError(TextView view, String text) {
        boolean isErrorVisible = text != null && !text.isEmpty();
        view.setVisibility(isErrorVisible ? View.VISIBLE : View.GONE);
        if(isErrorVisible){
            view.setText(text);
        }
    }

    @BindingAdapter("bind:afterWatcher")
    public static void bindAfterChange(TextView textView, OnAfterTextChangeListener listener) {
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listener.onAfterChange(s.toString());
            }
        });
    }

    public interface OnAfterTextChangeListener{
        void onAfterChange(String value);
    }}
