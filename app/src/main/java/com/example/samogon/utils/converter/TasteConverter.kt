package com.example.samogon.utils.converter

import androidx.room.TypeConverter
import com.example.samogon.data.local.model.Taste
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlinx.metadata.Flag
import java.util.*


class TasteConverter {
    var moshi = Moshi.Builder().build()
    val type = Types.newParameterizedType(
        MutableList::class.java,
        Taste::class.java
    )
    val jsonAdapter: JsonAdapter<MutableList<Taste>> = moshi.adapter(type)

    @TypeConverter
    fun stringToObjectList(data: String?): MutableList<Taste>? {
        if (data == null) {
            return arrayListOf()
        }

        return jsonAdapter.fromJson(data)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: MutableList<Taste>?): String {

        val jsonAdapter = moshi.adapter<List<Taste>>(type)
        return jsonAdapter.toJson(someObjects)
    }
}