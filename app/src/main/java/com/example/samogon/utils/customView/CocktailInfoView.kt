package com.example.samogon.utils.customView

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import kotlinx.android.synthetic.main.view_cocktail_info.view.*
import kotlinx.android.synthetic.main.view_cocktail_preview.view.*
import kotlinx.android.synthetic.main.view_cocktail_preview.view.card
import kotlinx.android.synthetic.main.view_cocktail_preview.view.cocktail_image
import kotlinx.android.synthetic.main.view_cocktail_preview.view.cocktail_name

class CocktailInfoView @JvmOverloads constructor(context: Context,
                                                 attrs: AttributeSet? = null,
                                                 defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_cocktail_info, this, true)
    }

    val imageView: ImageView = cocktail_image
    val name: TextView = cocktail_name

    fun attachCocktail(cocktail: Cocktail?) {
        cocktail?.let {
            loadImage(it.image)
            name.text = cocktail.name
            it.difficulty?.let {
                difficulty.text = it.name
            }
            it.strength?.let {
                strength.text = it.name
            }
            time.text = "${cocktail.preparationTime} min"
        }
    }

    fun loadImage(url: String?) {
        Glide.with(context)
            .asBitmap()
            .load(url)
            .transform(RoundedCorners(32))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.default_glassware)
            .error(R.drawable.default_glassware)
            .listener(object : RequestListener<Bitmap?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap?>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap?>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    if (resource != null) {
                        Palette.from(resource).generate { palette: Palette? ->
                            if (palette != null) {
                                attachPalette(palette)
                            }
                        }
                    }
                    return false
                }
            }).into(imageView)
    }

    fun attachPalette(palette: Palette) {
        palette.vibrantSwatch?.let {
            card.setCardBackgroundColor(it.rgb)
            name.setTextColor(it.titleTextColor)
            difficulty.setTextColor(it.titleTextColor)
            strength.setTextColor(it.titleTextColor)
            time.setTextColor(it.titleTextColor)
            icon_time.setColorFilter(it.titleTextColor, android.graphics.PorterDuff.Mode.SRC_IN)
            icon_difficulty.setColorFilter(it.titleTextColor, android.graphics.PorterDuff.Mode.SRC_IN)
            icon_strength.setColorFilter(it.titleTextColor, android.graphics.PorterDuff.Mode.SRC_IN)
        }
    }
}