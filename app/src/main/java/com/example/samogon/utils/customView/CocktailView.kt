package com.example.samogon.utils.customView

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.ui.adapter.TasteAdapter
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.view_cocktail.view.*
import kotlinx.android.synthetic.main.view_cocktail_preview.view.*
import kotlinx.android.synthetic.main.view_cocktail_preview.view.cocktail_image
import kotlinx.android.synthetic.main.view_cocktail_preview.view.cocktail_name

class CocktailView @JvmOverloads constructor(context: Context,
                                             attrs: AttributeSet? = null,
                                             defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_cocktail, this, true)
    }

    private lateinit var tasteAdapter: TasteAdapter

    val imageView: ImageView = cocktail_image
    val name: TextView = cocktail_name
    val card: MaterialCardView = card_name

    fun attachCocktail(cocktail: Cocktail?) {
        cocktail?.let {
            loadImage(it.image)
            name.text = it.name
            initTasteAdapter(it)
        }
    }

    private fun initTasteAdapter(cocktail: Cocktail) {
        tasteAdapter = TasteAdapter()
        with(tastes) {
            adapter = tasteAdapter
            layoutManager = FlexboxLayoutManager(context).apply {
                flexDirection = FlexDirection.ROW
                flexWrap = FlexWrap.WRAP
                justifyContent = JustifyContent.FLEX_START
            }
        }
        tasteAdapter.submitList(cocktail.taste)
    }

    fun loadImage(url: String?) {
        Glide.with(context)
            .asBitmap()
            .load(url)
            .transform(RoundedCorners(32))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.default_glassware)
            .error(R.drawable.default_glassware)
            .listener(object : RequestListener<Bitmap?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap?>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap?>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    if (resource != null) {
                        Palette.from(resource).generate { palette: Palette? ->
                            if (palette != null) {
                                attachPalette(palette)
                            }
                        }
                    }
                    return false
                }
            }).into(imageView)
    }

    fun attachPalette(palette: Palette) {
        palette.vibrantSwatch?.let {
            card.setCardBackgroundColor(it.rgb)
            container_image.setCardBackgroundColor(it.rgb)
            name.setTextColor(it.titleTextColor)
        }
    }
}