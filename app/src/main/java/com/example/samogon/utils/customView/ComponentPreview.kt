package com.example.samogon.utils.customView

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.samogon.R
import com.example.samogon.data.local.model.Cocktail
import com.example.samogon.data.local.model.Component
import kotlinx.android.synthetic.main.view_cocktail_preview.view.*
import kotlinx.android.synthetic.main.view_cocktail_preview.view.card
import kotlinx.android.synthetic.main.view_component_preview.view.*

class ComponentPreview @JvmOverloads constructor(context: Context,
                                                 attrs: AttributeSet? = null,
                                                 defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_component_preview, this, true)
    }

    val imageView: ImageView = component_image
    val name: TextView = component_name
    val volume: TextView = component_volume

    fun attachComponent(component: Component?) {
        component?.let {
            loadImage(it.ingredient.image)
            name.text = it.ingredient.name
            volume.visibility = when(it.ingredient.isLiquid()) {
                true -> View.VISIBLE
                false -> View.INVISIBLE
            }
        }
    }

    fun loadImage(url: String?) {
        Glide.with(context)
            .asBitmap()
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.default_glassware)
            .error(R.drawable.default_glassware)
            .listener(object : RequestListener<Bitmap?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap?>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap?>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    if (resource != null) {
                        Palette.from(resource).generate { palette: Palette? ->
                            if (palette != null) {
                                attachPalette(palette)
                            }
                        }
                    }
                    return false
                }
            }).into(imageView)
    }

    fun attachPalette(palette: Palette) {
        palette.vibrantSwatch?.let {
            card.setCardBackgroundColor(it.rgb)
            name.setTextColor(it.titleTextColor)
            volume.setTextColor(it.bodyTextColor)
        }
    }
}