package com.example.samogon.utils.enums

enum class CocktailListType {
    COCKTAILS_ALL,
    COCKTAILS_FOR_INGREDIENT,
    COCKTAILS_FOR_COMPILATION
}