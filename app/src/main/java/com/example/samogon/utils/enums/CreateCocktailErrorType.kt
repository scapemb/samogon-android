package com.example.samogon.utils.enums

enum class CreateCocktailErrorType {
    EMPTY_NAME,
    EMPTY_DESCRIPTION,
    EMPTY_TIME,
    EMPTY_COMPONENTS,
    EMPTY_STEPS,
    TOO_LONG_NAME,
    TOO_LONG_DESCRIPTION,
    NONE
}