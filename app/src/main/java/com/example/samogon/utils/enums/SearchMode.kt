package com.example.samogon.utils.enums

enum class SearchMode {
    TEXT,
    FILTER
}