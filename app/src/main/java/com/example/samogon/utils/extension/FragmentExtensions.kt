package com.example.samogon.utils.extension

import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout

object FragmentExtensions {
    fun <T : CoordinatorLayout.Behavior<*>> View.findBehavior(): T = layoutParams.run {
        require(this is CoordinatorLayout.LayoutParams) { "View's layout params should be CoordinatorLayout.LayoutParams" }

        (layoutParams as CoordinatorLayout.LayoutParams).behavior as? T
            ?: throw IllegalArgumentException("Layout's behavior is not current behavior")
    }
}