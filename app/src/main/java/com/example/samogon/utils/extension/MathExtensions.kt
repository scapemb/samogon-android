package com.example.samogon.utils.extension

object MathExtensions {
    fun gcd(a: Int, b: Int): Int = if(b == 0 ) a else gcd(b, a%b)

    fun gcd(list: List<Int>): Int {
        if(list.isEmpty()) return 1
        if(list.size == 1) return list[0]

        var result = gcd(list[0], list[1])
        for(i in 2 until list.size ) {
            result = gcd(result, list[i])
        }
        return result
    }
}