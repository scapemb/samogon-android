package com.example.samogon.utils.listener

import com.example.samogon.data.local.model.Cocktail

interface CocktailClickListener {
    fun onClick(cocktail: Cocktail)
}