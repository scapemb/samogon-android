package com.example.samogon.utils.listener

import com.example.samogon.data.local.model.Compilation

interface CompilationClickListener {
    fun onClick(compilation: Compilation)
}