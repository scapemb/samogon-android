package com.example.samogon.utils.listener


interface OnClickListener {
    fun onClick()
}