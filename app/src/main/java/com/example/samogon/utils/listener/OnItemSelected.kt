package com.example.samogon.utils.listener


interface OnItemSelected<T> {
    fun onSelected(item: T)
}