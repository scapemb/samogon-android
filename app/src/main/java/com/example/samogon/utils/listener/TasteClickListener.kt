package com.example.samogon.utils.listener

import com.example.samogon.data.local.model.Taste

interface TasteClickListener {
    fun onClick(taste: Taste)
}