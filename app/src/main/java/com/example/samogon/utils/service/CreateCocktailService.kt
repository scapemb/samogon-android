package com.example.samogon.utils.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.NavDeepLinkBuilder
import com.example.samogon.R
import com.example.samogon.data.repo.SamogonRepo
import com.example.samogon.ui.navigation.NavActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import javax.inject.Inject

@AndroidEntryPoint
class CreateCocktailService: Service() {
    companion object {
        private val TAG = "CreateCocktailService"
        private val CREATE_CHANNEL_ID = "CREATE_CHANNEL_ID"
        private val SUCCESS_CHANNEL_ID = "SUCCESS_CHANNEL_ID"
        private val UPLOADING_ID = 38
        private val SUCCESS_ID = 37

        val ACTION_UPLOAD = "ACTION_UPLOAD"
        val ACTION_STOP = "ACTION_STOP"
    }

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    @Inject
    lateinit var repo: SamogonRepo

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand::action=" + intent?.action)
        if (intent?.action != null) {
            if (intent.action.equals(ACTION_UPLOAD)) {
                upload(this)
            }
            if (intent.action.equals(ACTION_STOP)) {
                stopService()
            }
        }
        return START_REDELIVER_INTENT
    }

    private fun upload(context: Context) {
        scope.launch {
            showUploadingNotification(context)
            val cocktail = repo.createCocktail("")
            delay(5000)
            showSuccessNotification(context, cocktail?.cocktailId)
        }
    }

    private fun showUploadingNotification(context: Context) {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel(CREATE_CHANNEL_ID, "Cocktail creating")
            } else {
                // If earlier version channel ID is not used
                ""
            }
        val notificationBuilder =
            NotificationCompat.Builder(context, channelId)
                .setContentText("Creating cocktail...")
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setProgress(0, 0, true)
                .setAutoCancel(true)
                .setOngoing(true)
        startForeground(UPLOADING_ID, notificationBuilder.build())
    }

    private fun showSuccessNotification(context: Context, cocktailId: String?) {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel(SUCCESS_CHANNEL_ID, "Cocktail result")
            } else {
                // If earlier version channel ID is not used
                ""
            }
        val pendingIntent = NavDeepLinkBuilder(context)
            .setComponentName(NavActivity::class.java)
            .setGraph(R.navigation.nav_graph)
            .setDestination(R.id.cocktailFragment)
            .setArguments(Bundle().apply {
                putString("cocktailId", cocktailId)
                putBoolean("isFromNotification", true)
            })
            .createPendingIntent()

        val notificationBuilder =
            NotificationCompat.Builder(context, channelId)
                .setContentText("New cocktail is ready!")
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setOngoing(false)

        startForeground(SUCCESS_ID, notificationBuilder.build())
    }

    private fun stopService() {
        stopForeground(true)
        stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }
}