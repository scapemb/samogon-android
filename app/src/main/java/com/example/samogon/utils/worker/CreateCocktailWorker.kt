package com.example.samogon.utils.worker

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.navigation.NavDeepLinkBuilder
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import com.example.samogon.R
import com.example.samogon.data.repo.FileRepo
import com.example.samogon.data.repo.SamogonRepo
import com.example.samogon.ui.navigation.NavActivity
import com.example.samogon.utils.service.CreateCocktailService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import java.io.File
import javax.inject.Inject


class CreateCocktailWorker @WorkerInject constructor(@Assisted context: Context, @Assisted params: WorkerParameters, val repo: SamogonRepo, val fileRepo: FileRepo): CoroutineWorker(context, params) {
    companion object {
        private val CREATE_CHANNEL_ID = "CREATE_CHANNEL_ID"
        private val SUCCESS_CHANNEL_ID = "SUCCESS_CHANNEL_ID"
        private val UPLOADING_ID = 38
        private val SUCCESS_ID = 37

        val ACTION_UPLOAD = "ACTION_UPLOAD"
        val ACTION_STOP = "ACTION_STOP"
    }

    override suspend fun doWork(): Result {
        setForeground(createForegroundInfo())
        val image = repo.imageForCreate.value
        val url = upload(image)
        val cocktail = repo.createCocktail(url)
        showResult(cocktail?.cocktailId)
        repo.clearCreation()
        return Result.success()
    }

    fun upload(image: File?): String {
        if(image != null) {
            return fileRepo.uploadImage(image).fileURL
        }
        return ""
    }

    private fun createForegroundInfo(): ForegroundInfo {
        val context = applicationContext

        val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(context, CREATE_CHANNEL_ID, "Cocktail creating")
        }else ""

        val notification: Notification = NotificationCompat.Builder(context, SUCCESS_CHANNEL_ID)
            .setContentTitle("Creating cocktail")
            .setContentText("just wait a little...")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setOngoing(true)
            .setChannelId(channelId)
            .build()

        return ForegroundInfo(UPLOADING_ID, notification)
    }

    private fun showResult(cocktailId: String?) {
        val context = applicationContext

        val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(context, SUCCESS_CHANNEL_ID, "Cocktail result")
        } else ""

        val pendingIntent = NavDeepLinkBuilder(context)
            .setComponentName(NavActivity::class.java)
            .setGraph(R.navigation.nav_graph)
            .setDestination(R.id.cocktailFragment)
            .setArguments(Bundle().apply {
                putString("cocktailId", cocktailId)
                //putBoolean("isFromNotification", true)
            })
            .createPendingIntent()

        val notification: Notification = NotificationCompat.Builder(context, SUCCESS_CHANNEL_ID)
            .setContentTitle("Cocktail is ready")
            .setContentText("Click to open info")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setOngoing(false)
            .setChannelId(channelId)
            .build()

        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(SUCCESS_ID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context, channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_DEFAULT)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        val service = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }
}